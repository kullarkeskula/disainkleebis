��    8      �  O   �      �     �     �     �     �     �     �                 �   "     �  
   �  9   �     	          (  	   G     Q     V  
   f  \   q  �   �     S     l  	   q     {     �     �     �     �     �     �     �     �     �       
   
          +      K     l     o     }     �     �     �  �   �  g   T	  �   �	  �   y
     $     ,     /  >   6     u  3  �     �     �     �     �     �     �     �     �        �        �     �  >   �               ,  
   J     U     [  
   n  ^   y  �   �     n     �  
   �     �     �     �     �     �               $  	   *  
   4     ?     F     O     d  !   �     �     �     �     �     �     �  �   �  f   �  �   �  �   �     n     v     y     �     �         8       6          5   ,   3             !       )                            0   	       1              '      4         7   %       &                      (           .   #                     +         
       /      -                              "   2       $      *          All Back Clicked Clicked Links Date Date and time Description Edit Email Failed to render template "%s". Please ensure the template cache folder "%s" exists and has write permissions. Terminated with error: "%s" Filter by link clicked First page For example, "Spring email". [link]Read the guide.[/link] From Google Analytics Campaign Google Analytics campaign name Last page Link Loading data... Loading... MailPoet Premium cannot start because it is missing core files. Please reinstall the plugin. MailPoet Premium plugin requires PHP version 5.3.3 or newer. Please read our [link]instructions[/link] on how to resolve this issue. Manage subscription link Name Next page No clicked links found No engagement data found Opened Preview in browser Previous page Read more on stats. Reply-to Save Sent to Stats Status Subscriber Subscriber Engagement This newsletter does not exist. This newsletter is not sent yet. To Unique clicks Unopened Unsubscribe link Unsubscribed View in browser link You have an older version of the Premium plugin. The features have been disabled in order not to break MailPoet. Please update it in the Plugins page now. You need to have MailPoet version %s or higher activated before using this version of MailPoet Premium. Your MailPoet Premium plugin is incompatible with the free MailPoet plugin. [link1]Register[/link1] your key or [link2]purchase one now[/link2] to update the Premium to the latest version. [link1]Register[/link1] your copy of the MailPoet Premium plugin to receive access to automatic upgrades and support. Need a license key? [link2]Purchase one now.[/link2] clicked of opened segments per page (screen options)Number of segments per page unsubscribed Project-Id-Version: 
Report-Msgid-Bugs-To: http://support.mailpoet.com/
POT-Creation-Date: 2017-11-14 19:39:59+00:00
PO-Revision-Date: 2017-MO-DA HO:MI+ZONE
Last-Translator: Christian Olsen <christian@vi4.no>, 2017
Language-Team: Norwegian Bokmål (Norway) (https://www.transifex.com/wysija/teams/16808/nb_NO/)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Language: nb_NO
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: grunt-wp-i18n 0.5.2
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: UTF-8
X-Textdomain-Support: yes
 Alle Tilbake Klikket Klikkete lenker Dato Dato og tid Beskrivelse Rediger E-post Kunne ikke lagre mal %s. Vennligst vær sikker på at malbuffermappen -template cache folder- %s eksisterer og har skriverettigheter. Avsluttet med feil: "%s" Filtrer etter lenke klikket Første side For eksempel, "epost sommer". [link]Les bruksanvisning.[/link] Fra Google Analytics kampanje Google analytics kampanjenavn Siste side Lenke Laster inn data... Laster inn MailPoet Premium kan ikke starte fordi det mangler viktige filer. Installer pluginet på nytt. MailPoet Premium plugin krever PHP versjon 5.3.3 eller nyere. Vennligst les våre [link] instruksjoner [/ link] om hvordan du løser dette problemet. Administrer abonnementslenke Navn Neste side Ingen klikkete lenker funnet Ingen engasjementsdata funnet Åpnet Forhåndsvis i nettleser Forrige side Les mer om statistikken Svar til Lagre Sendt til Statistikk Status Abonnent Abonnent-engasjement Nyhetsbrevet eksisterer ikke. Nyhetsbrevet er ikke sendt ennå. Til Unike klikk Uåpnet Avmeldingslenke Avmeldt Se i nettleserlenke Du har en eldre versjon av MailPoet Premium plugin. Funksjonene er deaktivert for ikke å ødelegge MailPoet. Vennligst oppdater den via Plugins-siden nå. Du må ha MailPoet versjon %s eller nyere aktivert før du bruker denne versionen av MailPoet Premium. MailPoet Premium-plugin er inkompatibel med gratis-versjonen MailPoet-plugin. [link1]Registrer[/link1 nøkkelen din eller [link2] kjøp en nå [/ link2] for å oppdatere Premium til den nyeste versjonen. [link1]Registrer[/ link1] din kopi av MailPoet Premium-plugin for å få tilgang til automatiske oppgraderinger og støtte. Trenger du en lisensnøkkel? [link2]Kjøp en nå.[/ link2] Klikket av Åpnet Antall segmenter per side Avmeldt 