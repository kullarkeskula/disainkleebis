<?php
/**
 * Template Name: Landing page
 */
get_header(2); ?>
<section class="header linebetween specialcolor rightline reverse">
  <a class="navbar-brand relative inlineblock" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
    <?php if (get_theme_mod( 'logo' )) {?>
    <img src="<?php echo get_theme_mod( 'logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    <?php   } else {?>
    <img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    <?php }?>
  </a>
  <div class="bg-holder parallax-windowmin" data-width="1920" data-height="813" data-parallax="3d">
    <img src="<?php echo get_field('hero_image')['url']; ?>" class="parallax" alt="<?php echo get_field('hero_image')['alt']; ?>">
    <div class="centeralign">
    </div>
    <div class="absolutehead">
      <div class="table">
        <div class="tr">
          <div class="td fontcolor">
            <div class="container">
              <?php if (get_field('header_promo')) {?>
              <h1 class="centeralign"><?php the_field('header_promo'); ?></h1>
              <br>
              <?php }?>
              <?php if (get_field('header_extra')) {?>
              <h4 class="centeralign fontcolor"><?php the_field('header_extra'); ?></h4>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php if (get_field('intro1_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray1') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray1') == 'light') {
  $bgcolor = 'light-inverted';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="section frontpagepromo promo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('demo_text')) {?>
    <h2 class="centeralign"><?php the_field('demo_text'); ?></h2>
    <br>
    <?php }?>
    <?php if (get_field('promo_1')) {?>
    <h4 class="centeralign"><?php the_field('promo_1'); ?></h4>
    <?php }?>
    <?php if (get_field('promo_cta')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta'); ?></h5>
    <?php } ?>
  </div>
</section>
<?php }?>
<?php if (get_field('half_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray2') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray2') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">
        <?php if ( get_field('halfimg1')){?>   
        <div class="half td middle">
          <div class="minheight">
            <div class="bgimg1 absolute" style="background-image: url(<?php echo get_field('halfimg1')['url'];?>);">
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="half td middle">
          <div class="halfcontent leftpadd leftalign inlineblock">
           <?php if (get_field('half_promo1')){?>
           <h3><?php the_field('half_promo1'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text1')){?>
           <h4><?php the_field('half_text1'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta1')){?>
           <h5 class="normal block"><?php the_field('half_cta1'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton1')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton1') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text') ;?></a>
           <?php } ?>
         </div>
       </div>
     </div>
   </div>

 </div>
</section>
<?php } ?>


<?php if (get_field('half_visibility2') == 'extra_visible' ) {?>
<?php
if (get_field('gray3') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray3') == 'light') {
  $bgcolor = 'light-inverted';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">

        <div class="half td middle">
          <div class="halfcontent rightpadd leftalign inlineblock">
           <?php if (get_field('half_promo2')){?>
           <h3><?php the_field('half_promo2'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text2')){?>
           <h4><?php the_field('half_text2'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta2')){?>
           <h5 class="normal block"><?php the_field('half_cta2'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton2')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton2') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text2') ;?></a>
           <?php } ?>
         </div>
       </div>
       <?php if ( get_field('halfimg2')){?>   
       <div class="half td middle">
        <div class="minheight">
          <div class="bgimg2 absolute" style="background-image: url(<?php echo get_field('halfimg2')['url'];?>);">

          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>

</div>
</section>
<?php } ?>
<?php if (get_field('intro2_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray4') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray4') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'invertmedium';
}
?>
<section class="section frontpagepromo promo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('demo_text2')) {?>
    <h2 class="centeralign"><?php the_field('demo_text2'); ?></h2>
    <br>
    <?php }?>
    <?php if (get_field('promo_2')) {?>
    <h4 class="centeralign"><?php the_field('promo_2'); ?></h4>
    <?php }?>
    <?php if (get_field('promo_cta2')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta2'); ?></h5>
    <?php } ?>
  </div>
</section>
<?php }?>
<?php if (get_field('intro3_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray5') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray5') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="section frontpagepromo promo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('demo_text3')) {?>
    <h2 class="centeralign"><?php the_field('demo_text3'); ?></h2>
    <br>
    <?php }?>
    <?php if (get_field('promo_3')) {?>
    <h4 class="centeralign"><?php the_field('promo_3'); ?></h4>
    <?php }?>
    <?php if (get_field('promo_cta3')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta3'); ?></h5>
    <?php } ?>
  </div>
</section>
<?php }?>

<?php if (get_field('img_visibility') == 'extra_visible' ) {?>
<section class="relative linebetween specialcolor rightline reverse">
  <div class="bg-holder parallax-windowmin" data-width="1920" data-height="813" data-parallax="3d">
    <img src="<?php echo get_field('imgblock_image')['url']; ?>" class="parallax" alt="<?php echo get_field('imgblock_image')['alt']; ?>">
    <div class="absolutehead">
      <div class="table">
        <div class="tr">
          <div class="td fontcolor">
            <div class="container">
              <?php if (get_field('middle_promo')) {?>
              <h2 class="centeralign"><?php the_field('middle_promo'); ?></h2>
              <br>
              <?php }?>
              <?php if (get_field('middle_extra')) {?>
              <h4 class="centeralign fontcolor"><?php the_field('middle_extra'); ?></h4>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php }?>
<?php if (get_field('show_arguments') && get_field('show_arguments') == 'arguments_rm') {;}
else{
  ?>
  <?php
  if (get_field('gray6') == 'light') {
    $bgcolor = 'light-inverted';
  } elseif (get_field('gray6') == 'dark') {
    $bgcolor = 'dark-inverted';
  } else {
    $bgcolor = 'invertmedium';
  }
  ?>
  <section class="section arguments darkgray <?php echo $bgcolor; ?> fontcolor">
    <div class="minicontainer">
      <?php if (get_field('arguments_text')) {?>
      <h2 class="centeralign"><?php the_field('arguments_text'); ?></h2>
      <?php }?>
      <?php if (get_field('arguments_promo')) {?>
      <h4 class="centeralign"><?php the_field('arguments_promo'); ?></h4>
      <br>
      <?php }?>
      <?php
      if( have_rows('arguments') ):
    // loop through the rows of data?>
  <ul>
   <?php   while ( have_rows('arguments') ) : the_row();?>
     
    <li class="half column floatleft">
      <span class="checked fontcolor relative"> <i class="fa fa-check" aria-hidden="true"></i></span>
      <?php the_sub_field('row'); ?>
    </li>
    <li class="half column floatleft">
      <span class="checked fontcolor relative"> <i class="fa fa-check" aria-hidden="true"></i></span>
      <?php the_sub_field('row2'); ?>
    </li>
  <?php endwhile;?>
</ul>

<div class="clear"></div>
<?php endif;?>
</div>
</section>
<?php } ?>
<?php if (get_field('show_packages')) {?>
<?php
if (get_field('gray7') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray7') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="section <?php echo $bgcolor; ?> packages">
  <div class="minicontainer">
   <?php if (get_field('pack_title')) {?>
   <h2 class="centeralign"><?php the_field('pack_title'); ?></h2>
   <?php }?>
   <?php if (get_field('pack_intro')) {?>
   <h4 class="centeralign"><?php the_field('pack_intro'); ?></h4>
   <br>
   <?php }?>
   <div>
     <?php   while ( have_rows('packages') ) : the_row();?>
       <div class="one-forth column centeralign relative">
         <div class="bg">
          <div class="head centeralign">
           <h3 class="specialborder uppercase"> <?php  the_sub_field('pack_name');?></h3>
           <h4 class="fontcolor"><?php  the_sub_field('pack_price');?></h4>
         </div>
         <div class="services">
                  <?php // check for rows (sub repeater)
                  if( have_rows('services') ): ?>
                  <ul>
                    <?php 

                // loop through rows (sub repeater)
                    while( have_rows('services') ): the_row();
                    ?>
                    <li><?php the_sub_field('service'); ?></li>
                  <?php endwhile; ?>
                </ul>
              <?php endif; ?>

            </div>
            <div class="absolute">
             <a href="<?php the_sub_field('buy_link'); ?>" class="button  specialborder fillhover specialcolor centeralign"><?php _e('TELLI', 'mustmuna2017')?> </a>   
           </div>
         </div>     
       </div>       
     <?php endwhile;?>
     <div class="clear"></div></div>
   </div>
 </section>
 <?php } ?>
 <?php if (get_field('show_portfolio') && get_field('show_portfolio') == 'replace') {?>
 <?php
 if (get_field('gray8') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray8') == 'light') {
  $bgcolor = 'light-inverted';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<div class="portfolio mediumgray linebetween specialcolor leftline <?php echo $bgcolor; ?>">
  <div class="fullcontainer">
    <?php
    $args = array(
      'post_type' => 'portfolio',
      'posts_per_page' =>8,
      );
    $the_query = new WP_Query( $args );
// The Loop
    if ( $the_query->have_posts() ) {
      echo '<div class="items">';
      $counter=0;
      while ( $the_query->have_posts() ) {
        $class = ($counter >= 4) ? 'desktop' : '' ;
        $the_query->the_post();?>
        <div class="item <?php echo $class ?>">
          <?php $thumbnail_id = get_post_thumbnail_id( get_the_ID() );
          $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);  ?>
          <a class="image group" rel="group" href="<?php the_post_thumbnail_url( 'large' ) ?>"><img class="gray" src="<?php the_post_thumbnail_url( 'thumbnail' ) ?>" alt="<?php echo $alt ;?>"/></a>
        </div>
        <?php  $counter++;
      }?>
      <?php echo '</div>';
      /* Restore original Post Data */
      wp_reset_postdata();
    }
    ?>
    <div class="clear"></div>
  </div>  
</div>
<?php  } else {?>

<section class="relative linebetween specialcolor rightline reverse">
  <div class="bg-holder parallax-windowmin" data-width="1920" data-height="813" data-parallax="3d">
    <img src="<?php echo get_field('imgblock_image2')['url']; ?>" class="parallax" alt="<?php echo get_field('imgblock_image2')['alt']; ?>">
    <div class="absolutehead">
      <div class="table">
        <div class="tr">
          <div class="td fontcolor">
            <div class="container">
              <?php if (get_field('middle_promo2')) {?>
              <h2 class="centeralign"><?php the_field('middle_promo2'); ?></h2>
              <br>
              <?php } ?>
              <?php if (get_field('middle_extra2')) {?>
              <h4 class="centeralign fontcolor"><?php the_field('middle_extra2'); ?></h4>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php }?>
<?php if (get_field('half_visibility3') == 'extra_visible' ) {?>
<?php
if (get_field('gray9') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray9') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">
        <?php if ( get_field('halfimg3')){?>   
        <div class="half td middle">
          <div class="minheight">
            <div class="bgimg1 absolute" style="background-image: url(<?php echo get_field('halfimg3')['url'];?>);"></div>
          </div>
        </div>
        <?php } ?>
        <div class="half td middle">
          <div class="halfcontent leftpadd leftalign inlineblock">
           <?php if (get_field('half_promo3')){?>
           <h3><?php the_field('half_promo3'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text3')){?>
           <h4><?php the_field('half_text3'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta3')){?>
           <h5 class="normal block"><?php the_field('half_cta3'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton3')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton3') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text3') ;?></a>
           <?php } ?>
         </div>
       </div>
     </div>
   </div>

 </div>
</section>
<?php } ?>


<?php if (get_field('half_visibility4') == 'extra_visible' ) {?>
<?php
if (get_field('gray10') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray10') == 'light') {
  $bgcolor = 'light-inverted';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">

        <div class="half td middle">
          <div class="halfcontent rightpadd leftalign inlineblock">
           <?php if (get_field('half_promo4')){?>
           <h3><?php the_field('half_promo4'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text4')){?>
           <h4><?php the_field('half_text4'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta4')){?>
           <h5 class="normal block"><?php the_field('half_cta4'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton4')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton4') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text4') ;?></a>
           <?php } ?>
         </div>
       </div>
       <?php if ( get_field('halfimg4')){?>   
       <div class="half td middle">
        <div class="minheight">
          <div class="bgimg2 absolute" style="background-image: url(<?php echo get_field('halfimg4')['url'];?>);"></div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>

</div>
</section>
<?php } ?>
<?php
if (get_field('gray11') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray11') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'invertmedium';
}
?>
<section class="section <?php echo $bgcolor; ?>">
  <div class="minicontainer">
    <?php if (get_field('clients_title')) {?>
    <h2 class="centeralign"><?php the_field('clients_title'); ?></h2>
    <?php } ?>
    <?php if (get_field('custmers_sub', 'option')) {?>
    <h4 class="centeralign"><?php the_field('custmers_sub', 'option'); ?></h4>
    <br>
    <?php } ?>
    <?php if ( get_field('show_quotes')) {?>
    <div id="clientssowl" class="owl-carousel owl-theme">
      <?php
  // loop through the rows of data
      while ( have_rows('client_testimonials') ) : the_row();?>
      <?php if (get_sub_field('publish')) {?>
      <div class="client thumb">
        <div class="client_wrapper">
          <?php echo get_sub_field('text');?>
          <div class="authors specialborder">
            <span class="client">
              <?php _e('Nimi:', 'mustmuna2017')?> 
              <span class="gray"> <?php echo get_sub_field('name'); ?></span>
              <span class="slash desktop"><?php echo " / ";?></span>
            </span>
            <span class="client">
             <?php _e('Ametikoht:', 'mustmuna2017')?>
             <span class="gray">
               <?php echo get_sub_field('position'); ?> 
             </span>
           </span>
         </div>
       </div>
     </div>
     <?php  } ?>
     
   <?php endwhile;?>
 </div>
 <?php } ?>

 <?php
 if( have_rows('client_logos', 'option') ):?>
 <div id="clientlogos" class="owl-carousel owl-theme">
  <?php
  // loop through the rows of data
  while ( have_rows('client_logos', 'option') ) : the_row();
  $caturl = get_sub_field('cat_url', 'option');
  ?>
  <div class="client thumb specialhover">
    <span class="client_wrapper">
      <a href="<?php echo $caturl  ;?>">
        <img class="gray" src="<?php echo get_sub_field('logo', 'option')['url'];?>" alt="<?php echo get_sub_field('logo', 'option')['alt'];?>">
      </a>
    </span>
  </div>
<?php endwhile;?>
</div>
<?php endif;?>
</div>
</section>
<?php
if (get_field('gray12') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray12') == 'light') {
  $bgcolor = 'light-inverted';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="section mediumgray <?php echo $bgcolor; ?> centeralign fontcolor">
  <div class="container">
    <div class="call_action">
      <?php if (get_field('contact')) {?>
      <?php the_field('contact'); ?>
      <br>
      <br>
      <?php } ?>
      <?php the_field('form'); ?> 
    </div>
  </div>
</section>
<?php get_footer(); ?>