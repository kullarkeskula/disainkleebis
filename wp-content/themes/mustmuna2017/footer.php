<?php
if ( !is_page_template( 'services-template.php' ) && !is_page_template( 'services-template.php-other' ) && !is_page_template( 'contact-template-otherdom.php' )  && !is_page_template( 'contact-template.php' )  && !is_page_template( 'landing-template.php' ) && !is_page_template( 'landing-packages.php' ) && !is_single() && !is_home()  ) {
	?>
	
	<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Roboto:300,400,500,900' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>
  
  
	
	<section class="section clients darkgray light-inverted fontcolor">
		<div class="container">
			<div class="customerslogan">
			<?php if (get_field('custmers_slogan', 'option')) {?>
				<h2 class="centeralign hsize"><?php the_field('custmers_slogan', 'option'); ?></h2>
				<br>
				<?php } ?>
				<?php if (get_field('custmers_sub', 'option')) {?>
				<h4 class="centeralign"><?php the_field('custmers_sub', 'option'); ?></h4>
				<?php } ?>
			</div>
			<?php
			if( have_rows('client_logos', 'option') ):?>
			<div id="logosowl" class="owl-carousel owl-theme">
				<?php
 	// loop through the rows of data
				while ( have_rows('client_logos', 'option') ) : the_row();
				$caturl = get_sub_field('cat_url', 'option');
				?>
				<div class="client thumb">
					<span class="client_wrapper specialhover">
						<a href="<?php echo $caturl  ;?>">
							<img class="gray" src="<?php echo get_sub_field('logo', 'option')['url'];?>" alt="<?php echo get_sub_field('logo', 'option')['alt'];?>">
						</a>
					</span>
				</div>
			<?php endwhile;?>
		</div>
	<?php endif;?>
</div>
</section>
<?php 
} 
?>
<?php if ( is_page_template( 'frontpage-template-otherdom.php' ) || is_home() || is_page_template( 'services-template-other.php' ) || is_page_template( 'services-template.php' ) || is_single()  || is_page_template( 'portfolio-template.php' ) || is_page_template( 'portfolio-template-trykised.php' ) ):?>
<section class="servicespage calltoaction mediumgray dark-inverted centeralign fontcolor">
	<div class="container">
		<div class="call_action">
					<?php if (get_field('call_action', 'option')) {?>

					<?php echo is_page_template( 'portfolio-template-trykised.php' ) ?  "<h1>" : '<h2>' ?>
			<?php the_field('call_action', 'option'); ?>
				<?php echo is_page_template( 'portfolio-template-trykised.php' ) ?  "</h1>" : '</h2>' ?>
						<?php } ?>	
			<?php if (get_field('sub_cta', 'option')) {?>
			<h4>
				 <?php the_field('sub_cta', 'option'); ?>
				 </h4>
			<?php } ?>			
			<?php the_field('form', 'option'); ?> 
		</div>
	</div>
</section>
<?php endif;?>
<footer class="footer lightgray invertmedium fontcolor">
	<div class="copyright">
		<div class="container">
			<div class="centeralign minheight">
				<span id="back_to_top" class="button_js bordercolor">
					<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					<i class="fa fa-angle-up" aria-hidden="true"></i></span>
				</div>
				<div class="centeralign fontcolor">
					<?php the_field('copyright_et', 'option'); ?>
				</div>
			</div>
		</div>
	</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>