<?php

get_header(); ?>

<div class="section portfolio mediumgray  linebetween specialcolor leftline dark-inverted">
	<header class="header desktop fontcolor">
		<div class="container">
			<div class="all-tekst">
				<?php the_field('promo'); ?> 	
			</div>
			<?php
			$terms = get_terms([
				'taxonomy' => 'tag',
				'hide_empty' => false,
				]);?>
			<?php
			foreach ($terms as $term ) {?>
			<div class="specialborder fontcolor tagpromo" id="<?php echo $term->term_id;?>text">
				<?php 
				echo $term->description;
				?>		
			</div>
			<?php
		}
		?>
	</div>
</header>
<div class="container">
	<?php
	$qobj = get_queried_object();
	$args = array(
		'post_type' => 'portfolio',
		'orderby' => 'rand',
		'tax_query' => array(
			array(
				'taxonomy' => $qobj->taxonomy,
				'field' => 'id',
				'terms' => $qobj->term_id,
				)
			)
		);
	$the_query = new WP_Query( $args );
// The Loop
	if ( $the_query->have_posts() ) {
		echo '<div class="items">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();

			$tagargs = array( 'fields' => 'ids');
			$terms = wp_get_post_terms( get_the_ID(), 'tag', $tagargs );
			$terms = implode(' ', $terms);

			echo '<div class="item '.$terms.'">'; ?>
			<?php	$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
			$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);	?>

			<a class="image" href="<?php the_permalink(); ?>"><img class="gray" src="<?php the_post_thumbnail_url( 'thumbnail' ) ?>" alt="<?php echo $alt ;?>"/></a>
			<?php	echo '</div>';
		}?>
		<?php echo '</div>';
		/* Restore original Post Data */
		wp_reset_postdata();
	}
	?>
	<div class="clear"></div>
	<footer class="footertags tags fontcolor">
		<div class="tags">
			<?php
			$terms = get_terms([
				'taxonomy' => 'tag',
				'hide_empty' => false,
				]);?>

				<div class="sort currentcat specialborder fontcolor" id="all-f"><?php _e('Näita kõiki' ) ?></div>
				<?php
				foreach ($terms as $term ) {?>

				<div class="sort specialborder fontcolor" id="<?php echo $term->term_id;?>-f">
					<?php 
					echo $term->name;
					?>		
				</div>
				<?php
			}
			?>
			<div class="clear"></div>
		</div>
	</footer>
</div>		
</div>

<?php get_footer(); ?>