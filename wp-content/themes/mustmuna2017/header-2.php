<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
  <title><?php wp_title(); ?></title>
  <?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-106052120-4', 'auto');
  ga('send', 'pageview');

</script>

<?php $headcta = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'vorm' ?>

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    ga('send', 'event', '<?php echo $headcta ?>', 'submit', 'kontaktivorm');
}, false );
</script>
</head>
<body <?php body_class(); ?>>
  <div id="page" class="site">


