<?php
/**
 * Template Name: Alamlehtede esilehe templiit
 */
get_header(); ?>
<div class="header">
  <?php if( have_rows('hero') ):?>
   <div id="owl" class="owl-carousel owl-theme">
     <?php $counter = 1?>
     <?php   while ( have_rows('hero') ) : the_row();?>
       <div class="item" style="background-image: url(<?php echo get_sub_field('img')['url'];?>)">
        <img src="<?php echo get_sub_field('img')['url'];?>" alt="<?php echo get_sub_field('img')['alt'];?>">
        <div class="frontpromo absolute">
          <div class="table">
            <div class="tr">
              <div class="td fontcolor">  
                <div class="lg_container">
                  <?php $firstcta = $_SERVER['SERVER_NAME'].'-slaid-'.$counter ?>
                  <?php $firstlabel = 'Slaideri Nupp-'.$counter ?>
                  <?php  the_sub_field('promo');?>
                  <?php if (get_sub_field('cta')) {?>
                  <div class="<?php echo get_sub_field('alignment');?>"><a class="button maincolor" onclick="ga('send', 'event', '<?php echo $firstcta ?>' , 'Click', '<?php echo $firstlabel ?>');" href="<?php echo get_sub_field('btn_link'); ?>"><?php echo get_sub_field('cta'); ?></a></div>
                  <?php  } ?>
                </div>  
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php $counter++; ?>
    <?php endwhile;?>
  </div>
<?php endif;?>
</div>
<?php
if (get_field('gray1') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray1') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'invertmedium';
}
?>
<section class="section frontpagepromo promo mediumgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('demo_text')){?>
    <h1 class="centeralign"><?php the_field('demo_text'); ?></h1>
    <br>
    <?php } ?>
    <?php if (get_field('promo_1')){?>
    <h4 class="centeralign"><?php the_field('promo_1'); ?></h4>
    <?php } ?>
    <?php if (get_field('promo_cta')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta'); ?></h5>
    <?php } ?>
  </div>
</section>

<?php if (get_field('half_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray2') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray2') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">
        <?php if ( get_field('halfimg1')){?>   
        <div class="half td middle">
          <div class="minheight">
            <div class="bgimg1 absolute" style="background-image: url(<?php echo get_field('halfimg1')['url'];?>);">
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="half td middle">
          <div class="halfcontent leftpadd leftalign inlineblock">
           <?php if (get_field('half_promo1')){?>
           <h3><?php the_field('half_promo1'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text1')){?>
           <h4><?php the_field('half_text1'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta1')){?>
           <h5 class="normal block"><?php the_field('half_cta1'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton1')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton1') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text') ;?></a>
           <?php } ?>
         </div>
       </div>
     </div>
   </div>

 </div>
</section>
<?php } ?>


<?php if (get_field('half_visibility2') == 'extra_visible' ) {?>
<?php
if (get_field('gray3') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray3') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'invertmedium';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">

        <div class="half td middle">
          <div class="halfcontent rightpadd leftalign inlineblock">
           <?php if (get_field('half_promo2')){?>
           <h3><?php the_field('half_promo2'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text2')){?>
           <h4><?php the_field('half_text2'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta2')){?>
           <h5 class="normal block"><?php the_field('half_cta2'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton2')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton2') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text2') ;?></a>
           <?php } ?>
         </div>
       </div>
       <?php if ( get_field('halfimg2')){?>   
       <div class="half td middle">
        <div class="minheight">
          <div class="bgimg2 absolute" style="background-image: url(<?php echo get_field('halfimg2')['url'];?>);">

          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>

</div>
</section>
<?php } ?>
<?php if (get_field('extra_visibility2') == 'extra_visible' ) {?>
<?php
if (get_field('gray4') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray4') == 'medium') {
  $bgcolor = 'invertmedium';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="section frontpagepromo promo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('product_text')){?>
    <h1 class="centeralign"><?php the_field('product_text'); ?></h1>
    <br>
    <?php } ?>
    <?php if (get_field('promo_extra')){?>
    <h4 class="centeralign"><?php the_field('promo_extra'); ?></h4>
    <?php } ?>
    <?php if (get_field('promo_cta2')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta2'); ?></h5>
    <?php } ?>
  </div>
</section>
<?php } ?>
<?php if (get_field('portfolio_visibility') && get_field('portfolio_visibility') == 'portfolio_rm' ) {
} else {?>
<?php
if (get_field('gray5') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray5') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'invertmedium';
}
?>
<section class="section portfolio mediumgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
   <?php if (get_field('portfolio_text')) {?>
   <div class="portfoliohead fontcolor">
    <h4 class="centeralign"><?php the_field('portfolio_text'); ?></h4>        
  </div>
  <?php } ?>
  <?php 
  $args = array(
    'post_type' => 'portfolio',
    'post_count' => '12',
    'orderby'   => 'rand',
    'posts_per_page' => 12
    );
  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) { ?>
  <ul class="portfolio">
   <?php  while ( $the_query->have_posts() ) {
    $the_query->the_post();?>
    <li class="thumbnail">
     <div class="image">
      <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail('thumbnail'); ?>
      </a>
    </div>
  </li>
  <?php } ?>
</ul>
<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php } ?>
<div class="clear"></div>
<div class="centeralign">
  <a href="<?php the_field('portfolio_url'); ?>" class="button smallbutton fillhover specialcolor">
   <?php if (get_field('btn_text')) {
    the_field('btn_text'); 
  } else {
   _e('Vaata veel', 'mustmuna2017');
 }?> 
</a>
</div>
</div>
</section>
<?php } ?>
<?php if (get_field('extra') && get_field('extra_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray6') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray6') == 'medium') {
  $bgcolor = 'invertmedium';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="section darkgray <?php echo $bgcolor; ?> fontcolor">
 <div class="minicontainer">
   <?php the_field('extra'); ?>
 </div>
</section>
<?php } ?>

<?php if (get_field('blog_visibility') == 'extra_visible' ) {?>
<?php
if (get_field('gray7') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray7') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="section frontportblog lightgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php 
    $args = array(
     'post_type' => 'post',
     'post_count' => '4',
     'orderby'   => 'rand',
     'posts_per_page' => 4
     );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {?>
    <div class="portfoliohead fontcolor">
      <?php if (get_field('blog_head')) {?>
      <h2 class="centeralign"><?php the_field('blog_head'); ?></h2>
      <?php } ?>
      <?php if (get_field('blog_text')) {?>
      <h4 class="centeralign"><?php the_field('blog_text'); ?></h4>
      <?php } ?>
    </div>
    <div class="blog headerblog">
      <?php while ( $the_query->have_posts() ) {
        $the_query->the_post(); ?>
        <div class="thumbnail">
          <a class="fontcolor hovercolor" href="<?php the_permalink(); ?>">
            <div class="relative">
              <?php if (has_post_thumbnail()) {
                the_post_thumbnail('thumbnail');
              } else {?>
              <img src="<?php echo get_template_directory_uri() . '/images/thumb-330-263.jpg';?>" alt="placeholder">
              <?php   } ?>
              <p class="absolute"><?php the_title(); ?></p>
            </div>
            <div class="whitebg excerpt"> 
              <?php echo excerpt(22); ?>
              <div class="visibleonhover readmorecolumn">
                <div class="table">
                  <div class="tr">
                    <div class="td verticalmiddle">
                      <i class="fa fa-angle-right specialcolor" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
                
              </div>
            </div> 
            
          </a>
        </div>
        <?php }; ?>
      </div>
      <div class="clear"></div>
      <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
      <?php }; ?>
    </div>
  </section>
  <?php } ?>
  <?php if (get_field('extra_visibility3') == 'extra_visible' ) {?>
  <?php
  if (get_field('gray8') == 'light') {
    $bgcolor = 'light-inverted';
  } elseif (get_field('gray8') == 'dark') {
    $bgcolor = 'dark-inverted';
  } else {
    $bgcolor = 'invertmedium';
  }
  ?>
  <section class="section frontpagepromo promo mediumgray <?php echo $bgcolor; ?> fontcolor">
    <div class="container">
      <?php if (get_field('secondproduct_text')){?>
      <h1 class="centeralign"><?php the_field('secondproduct_text'); ?></h1>
      <br>
      <?php } ?>
      <?php if (get_field('secondpromo')){?>
      <h4 class="centeralign"><?php the_field('secondpromo'); ?></h4>
      <?php } ?>
      <?php if (get_field('promo_cta3')){?>
      <h5 class="normal block centeralign"><?php the_field('promo_cta3'); ?></h5>
      <?php } ?>
    </div>
  </section>
  <?php } ?>
  <?php if ( get_field('show_quotes')) {?>
  <?php
  if (get_field('gray9') == 'light') {
    $bgcolor = 'light-inverted';
  } elseif (get_field('gray9') == 'medium') {
    $bgcolor = 'invertmedium';
  } else {
    $bgcolor = 'dark-inverted';
  }
  ?>
  <section class="section frontpagepromo promo darkgray <?php echo $bgcolor; ?> fontcolor">
    <div class="container">
      <?php if (get_field('clients_title')) {?>
      <h2 class="centeralign"><?php the_field('clients_title'); ?></h2>
      <?php } ?>
      <?php if (get_field('custmers_sub')) {?>
      <h4 class="centeralign"><?php the_field('custmers_sub'); ?></h4>
      <br>
      <?php } ?>

      <div id="clientssowl" class="owl-carousel owl-theme">
      
        <?php
  // loop through the rows of data
        while ( have_rows('client_testimonials') ) : the_row();?>
        
        <?php if (get_sub_field('publish')) {?>
         <div class="container">
        <div class="client thumb">
          <div class="client_wrapper">
            <?php echo get_sub_field('text');?>
            <div class="authors specialborder">
              <span class="client">
                <?php _e('Nimi:', 'mustmuna2017')?> 
                <span class="gray"> <?php echo get_sub_field('name'); ?></span>
                <span class="slash desktop"><?php echo " / ";?></span>
              </span>
              <span class="client">
               <?php _e('Ametikoht:', 'mustmuna2017')?>
               <span class="gray">
                 <?php echo get_sub_field('position'); ?> 
               </span>
             </span>
           </div>
         </div>
       </div>
          </div>
       <?php  } ?>
       
     <?php endwhile;?>


 </div>
</div>
</section>
<?php } ?>
<?php if (get_field('half_visibility3') == 'extra_visible' ) {?>
<?php
if (get_field('gray10') == 'medium') {
  $bgcolor = 'invertmedium';
} elseif (get_field('gray10') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'light-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">
        <?php if ( get_field('halfimg3')){?>   
        <div class="half td middle">
          <div class="minheight">
            <div class="bgimg1 absolute" style="background-image: url(<?php echo get_field('halfimg3')['url'];?>);"></div>
          </div>
        </div>
        <?php } ?>
        <div class="half td middle">
          <div class="halfcontent leftpadd leftalign inlineblock">
           <?php if (get_field('half_promo3')){?>
           <h3><?php the_field('half_promo3'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text3')){?>
           <h4><?php the_field('half_text3'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta3')){?>
           <h5 class="normal block"><?php the_field('half_cta3'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton3')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton3') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text3') ;?></a>
           <?php } ?>
         </div>
       </div>
     </div>
   </div>

 </div>
</section>
<?php } ?>


<?php if (get_field('half_visibility4') == 'extra_visible' ) {?>
<?php
if (get_field('gray11') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray11') == 'medium') {
  $bgcolor = 'invertmedium';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
  <div class="relative">
    <div class="table">
      <div class="tr">

        <div class="half td middle">
          <div class="halfcontent rightpadd leftalign inlineblock">
           <?php if (get_field('half_promo4')){?>
           <h3><?php the_field('half_promo4'); ?></h3>
           <?php } ?>
           <?php if (get_field('half_text4')){?>
           <h4><?php the_field('half_text4'); ?></h4>
           <?php } ?>
           <?php if (get_field('half_cta4')){?>
           <h5 class="normal block"><?php the_field('half_cta4'); ?></h5>
           <?php } ?>
           <?php if (get_field('showbutton4')== 'extra_visible'){?>
           <a href="<?php the_field('halfbutton4') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text4') ;?></a>
           <?php } ?>
         </div>
       </div>
       <?php if ( get_field('halfimg4')){?>   
       <div class="half td middle">
        <div class="minheight">
          <div class="bgimg2 absolute" style="background-image: url(<?php echo get_field('halfimg4')['url'];?>);"></div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>

</div>
</section>
<?php } ?>
<?php if (get_field('extra_visibility4') == 'extra_visible' ) {?>
<?php
if (get_field('gray12') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray12') == 'dark') {
  $bgcolor = 'dark-inverted';
} else {
  $bgcolor = 'invertmedium';
}
?>
<section class="section frontpagepromo promo lightgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('product_text4')){?>
    <h1 class="centeralign"><?php the_field('product_text4'); ?></h1>
    <br>
    <?php } ?>
    <?php if (get_field('promo_extra4')){?>
    <h4 class="centeralign"><?php the_field('promo_extra4'); ?></h4>
    <?php } ?>
    <?php if (get_field('promo_cta4')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta4'); ?></h5>
    <?php } ?>
  </div>
</section>
<?php } ?>
<?php if (get_field('extra_visibility5') == 'extra_visible' ) {?>
<?php
if (get_field('gray13') == 'light') {
  $bgcolor = 'light-inverted';
} elseif (get_field('gray13') == 'medium') {
  $bgcolor = 'invertmedium';
} else {
  $bgcolor = 'dark-inverted';
}
?>
<section class="section frontpagepromo promo mediumgray <?php echo $bgcolor; ?> fontcolor">
  <div class="container">
    <?php if (get_field('product_text5')){?>
    <h1 class="centeralign"><?php the_field('product_text5'); ?></h1>
    <br>
    <?php } ?>
    <?php if (get_field('promo_extra5')){?>
    <h4 class="centeralign"><?php the_field('promo_extra5'); ?></h4>
    <?php } ?>
    <?php if (get_field('promo_cta5')){?>
    <h5 class="normal block centeralign"><?php the_field('promo_cta5'); ?></h5>
    <?php } ?>
  </div>
</section>
<?php } ?>
<?php get_footer(); ?>