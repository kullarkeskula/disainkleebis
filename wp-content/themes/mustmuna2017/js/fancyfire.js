
jQuery( document ).ready(function() {
jQuery("a.group.image").fancybox({
    'transitionIn'  : 'elastic',
    'transitionOut' : 'elastic',
    'clickContent'    : 'false',
    'speedIn'   : 600, 
    'speedOut'    : 200, 
    'overlayShow' : true,
    'showNavArrows' : true,
    'showCloseButton' : false
  });
}
);