jQuery( document ).ready(function() {
	if (jQuery(window).width() > 900) {
		jQuery(".sticky-element").stick_in_parent({
			sticky_class: 'is_stucked',
			offset_top: 50,
			offset_bottom: 0,
			bottoming: true,
			inner_scrolling: false
		});
	}
	jQuery(window).on('resize', function(event) {
		if (jQuery(window).width() > 900) {
			jQuery(".sticky-element").stick_in_parent({
				sticky_class: 'is_stucked',
				offset_top: 50,
				bottoming: true,
				inner_scrolling: false
			});
		} else {
			jQuery(".sticky-element").trigger("sticky_kit:detach");
		}
	});
}
);