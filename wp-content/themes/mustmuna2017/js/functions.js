
jQuery( document ).ready(function() {

// jQuery(function() { jQuery('[name="your-name"]').focus(); });

jQuery("#owl").owlCarousel({
 items:1,
 loop:true,
 autoplay:true,
 autoplayTimeout:4500,
 autoplaySpeed:1700,
 navSpeed:2000,
 dotsSpeed:2000,
 dragEndSpeed:2000,
 nav : false,
 navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
 dots : true,
 autoplayHoverPause:false
});

jQuery("#logosowl").owlCarousel({
 items:6,
 loop:true,
 autoplay:true,
 autoplayTimeout:4500,
 autoplaySpeed:1500,
 navSpeed :1500,
 nav : false,
 dots : false,
 autoplayHoverPause:true,
 responsiveClass:true,
 responsive:{
  0:{
    items:1
  },
  500:{
    items:2
  },
  600:{
    items:4
  },
  1000:{
    items:6
  }
}

});
jQuery("#clientssowl").owlCarousel({
 items:1,
 loop:true,
 autoplay:true,
 autoplayTimeout:4500,
 autoplaySpeed:1500,
 navSpeed :1500,
 nav : false,
 dots : false,
 autoplayHoverPause:true,
 responsiveClass:true,
 responsive:{
  0:{
    items:1
  },
  500:{
    items:1
  },
  600:{
    items:1
  },
  1000:{
    items:1
  }
}

});
jQuery("#clientlogos").owlCarousel({
 items:6,
 loop:true,
 autoplay:true,
 autoplayTimeout:4500,
 autoplaySpeed:1500,
 navSpeed :1500,
 nav : false,
 dots : false,
 autoplayHoverPause:true,
 responsiveClass:true,
 responsive:{
  0:{
    items:1
  },
  500:{
    items:2
  },
  600:{
    items:4
  },
  1000:{
    items:6
  }
}
});
// Find all iframes
var $iframes = jQuery( "iframe" );

// Find &#x26; save the aspect ratio for all iframes
$iframes.each(function () {
  jQuery( this ).data( "ratio", this.height / this.width )
    // Remove the hardcoded width &#x26; height attributes
    .removeAttr( "width" )
    .removeAttr( "height" );
  });

// Resize the iframes when the window is resized
jQuery( window ).resize( function () {
  $iframes.each( function() {
    // Get the parent container&#x27;s width
    var width = jQuery( this ).parent().width();
    jQuery( this ).width( width )
    .height( width * jQuery( this ).data( "ratio" ) );
  });
// Resize to fix all iframes on page load.
}).resize();

jQuery( ".sort" ).on( "click", function() {
  var str = jQuery( this ).attr('id') ;
  var id = str.replace("-f", "");
  if (id == "all" ) {
    jQuery('.items .item').fadeIn('fast');
    jQuery('.sort').removeClass('currentcat');
    jQuery('#all, #all-f').addClass('currentcat');
    jQuery('.tagpromo').hide();
    jQuery('.all-tekst').fadeIn('fast');
    if(jQuery( window ).width()<=800){
      jQuery('html, body').animate({
        scrollTop: jQuery('.section.portfolio').offset().top
      }, 800);
    }
  }
  else {
    id_class = '.'+id;
    id_id = '#'+id;
    jQuery('.items .item').hide();
    jQuery('.tagpromo, .all-tekst').hide();
    jQuery(id_id+'text').fadeIn('fast');
    jQuery('.items').children( id_class ).fadeIn('fast');
    jQuery('.sort').removeClass('currentcat');
    jQuery('.sort'+id_id).addClass('currentcat');
    jQuery('.sort'+id_id+"-f").addClass('currentcat');
    jQuery('html, body').animate({
      scrollTop: jQuery('.section.portfolio').offset().top
    }, 800);
    
  }

});
jQuery('.pick_pg').click(function (event) {
   event.preventDefault();
   var id = jQuery(this).attr('id');
var page_id = '#pack_'+id;
jQuery(".packpage.current").removeClass("current"); 
jQuery(".pick_pg.current").removeClass("current"); 
  jQuery(this).addClass('current');
    jQuery(page_id).addClass('current');
});
jQuery('.packlink').click(function (event) { 
    event.preventDefault();

var id = jQuery(this).attr('id');
var img_id = '#img'+id;
var sum_id = '#sum'+id;

$currensum = jQuery(this).closest('.packpage').find('.sum');
$currenimg = jQuery(this).closest('.packpage').find('.pg_img');
$currenlink = jQuery(this).closest('.packpage').find('.packlink');

         
          $currensum.css('display', 'none').removeClass("current"); 
          $currenimg.removeClass("current");
          $currenlink.removeClass("current"); 
    jQuery(this).addClass('current');
     jQuery(sum_id).show(250).addClass('current');
    jQuery(img_id).addClass('current');
});
jQuery(window).scroll(function() {
 var scrollPosition = window.pageYOffset;
 var windowSize     = window.innerHeight;
 var bodyHeight     = document.body.offsetHeight;

 
 if (jQuery(document).scrollTop() > 100) {
  jQuery('.site-header').addClass('small');
     jQuery('#main').css("margin-top", "100px");
}
else {
  jQuery('.site-header').removeClass('small');
}
if (jQuery(document).scrollTop() > jQuery(document).height()*0.25) {
  jQuery('.contact-us-trigger').addClass('active');
}
else {
  jQuery('.contact-us-trigger').removeClass('active');
}
if(jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 200) {
        jQuery('.contact-us-trigger').addClass('colorized');
   }
else {
  jQuery('.contact-us-trigger').removeClass('colorized');
}


});
jQuery('#back_to_top').click(function() {      // When arrow is clicked
  jQuery('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
      }, 500);
});

jQuery( ".wpcf7-textarea" ).focus(function() {
  jQuery( ".wpcf7-textarea" ).addClass('lg');
});

jQuery('#navbar-toggle').click(function (event) {
  event.preventDefault();
  if(jQuery( window ).width()<=860){
    if(!jQuery('.main-menu').is(':visible')) {
      jQuery('.main-menu').slideDown('fast');
      jQuery('.main-menu').css('display','block');
    } else {
      jQuery('.main-menu').slideUp('fast');
    }}
    else{
      jQuery('.main-menu').css('display','block');
    }
  });

jQuery(window).resize(function() {

  if(jQuery(window).width()<860){
   jQuery('.main-menu').css('display','none'); 
   jQuery('#navbar-toggle').css('display','block');

 }
 else {
  jQuery('.main-menu').css('display','block');
  jQuery('#navbar-toggle').css('display','none');
}

});

}
);


jQuery(window).load(function(){
var maxheight = 0;

jQuery('.packages .column').each(function () {
    maxheight = (jQuery(this).height() > maxheight ? jQuery(this).height() : maxheight);
});

jQuery('.packages .column').height(maxheight);

});