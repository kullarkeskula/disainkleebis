// ajax for blog
jQuery( document ).ready(function() {

  var $content = jQuery('.ajax_posts');
  var $loader = jQuery('#more_posts');
  var ppp = 1;
  var offset = jQuery('#main').find('.post').length;
  
jQuery('#more_posts').click(function() {      // When arrow is clicked
 
  if (!($loader.hasClass('post_loading_loader') || $loader.hasClass('post_no_more_posts'))) {
    jQuery.ajax({
      type: 'POST',
      dataType: 'html',
      url: ajax_posts.ajaxurl,
      data: {
        'ppp': ppp,
        'offset': offset,
        'action': 'more_post_ajax'
      },
      beforeSend : function () {
        $loader.addClass('post_loading_loader').html('');
      },
      success: function (data) {
        var $data = jQuery(data);
        if ($data.length) {
          var $newElements = $data.css({ opacity: 0 });
          $content.append($newElements);
          $newElements.animate({ opacity: 1 });
          $loader.removeClass('post_loading_loader').html(ajax_posts.loadmore);
        } else {
          $loader.removeClass('post_loading_loader, hovercolor').addClass('post_no_more_posts').html(ajax_posts.noposts);
        }
      },
      error : function (jqXHR, textStatus, errorThrown) {
        $loader.html($.parseJSON(jqXHR.responseText) + ' :: ' + textStatus + ' :: ' + errorThrown);
        console.log(jqXHR);
      },
    });
  }
  offset += ppp;
  return false;
});

});
