<?php
/**
 * Template Name: Päringu templiit
 */
get_header(2); ?>

    <div class="mediumgray invertmedium centeralign site-header">
    <a class="navbar-brand relative inlineblock" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
        <?php if (get_theme_mod( 'logo' )) {?>
        <img src="<?php echo get_theme_mod( 'logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        <?php   } else {?>
        <img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        <?php }?>
      </a>
      </div>
  <section class="section mediumgray invertmedium centeralign fontcolor">
  <div class="minicontainer">
    <div class="call_action">
      <?php if (get_field('contact')) {?>
         <?php the_field('contact'); ?>
               <br>
      <br>
      <?php } ?>
      <?php the_field('form'); ?> 
    </div>
  </div>
</section>
  <?php get_footer(); ?>