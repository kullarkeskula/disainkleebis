<?php
get_header(); ?>
<div class="section fontcolor centeralign mediumgray dark-inverted">
	<header>
		<h1 class="page-title"><?php _e( '400', 'mustmuna2017' ); ?></h1>
	</header>
	<h4><?php _e( 'Lehte ei leitud', 'mustmuna2017' ); ?></h4>
</div>
<?php get_footer(); ?>
