<?php
get_header(); ?>
<main id="main" class="site-main">
    <div class="blog section darkgray light-inverted fontcolor">
      <div class="container">
          <div id="ajax_posts" class="row ajax_posts">
            <?php
            $postsPerPage = -1;
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => $postsPerPage
                );
            $loop = new WP_Query($args);
            $first = true;
            $postcount = 0;
            while ($loop->have_posts()) : $loop->the_post();
            ?>
            <?php if ( ($postcount % 2) == 0 ){ ?>
    
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="table">
                    <a class="fontcolor hovercolor relative" href="<?php the_permalink()?>">
                        <div class="tr">
                            <div class="td col half middle">        
                                <div class="entry-head rightalign">
                                    <?php
                                    if ($first) {
                                        the_title( '<h1 class="specialafter rightheading">','</h1>' );
                                    } else {
                                        the_title( '<h2 class="specialafter">','</h2>' );
                                    }           
                                    ?>
                                </div><!-- .entry-header -->
                                <div class="content">
                                    <h4 class="rightalign"><?php echo excerpt(20); ?>
                                        <span class="notablet"><span class="button round specialborder maincolor
                          specialcolor">
                        <i class="fa fa-angle-right specialcolor" aria-hidden="true"></i>
                         </span>
                         </span>
                                    </h4>
                        <span class="tablet">
                          <span class="button specialborder fillhover specialcolor
                          specialcolor absolute"> <?php _e('Loe veel', 'mustmuna2017') ?>
                         </span></span>   
                                </div>
                            </div>
                            <div class="td col half nopadding">
                               <div class="entry-image">
                                <div class="image">
                                    <?php if (has_post_thumbnail()) { 
                                    the_post_thumbnail('large'); ?>
<!--                                   <img src="http://localhost:8888/disainkleebis/wp-content/uploads/2018/07/aknakleebised-remax-3-1024x644.jpg">-->
                                   <?php } else {?>
                                    <img src="<?php echo get_template_directory_uri() . '/images/thumbnail-461x288.jpg';?>" alt="placeholder">
                                    <?php   } ?>
                                </div>
                            </div>
                            </div>
                        </div></a>
                    </div>
                    <div class="clear"></div>
                    <footer class="entry-footer">
                        <?php
                        edit_post_link(
                            sprintf(
                                /* translators: %s: Name of current post */
                                __( 'Muuda postitust<span class="screen-reader-text fontcolor hovercolo"> "%s"</span>', 'mustmuna2017' ),
                                get_the_title()
                                ),
                            '<span class="fontcolor hovercolor">',
                            '</span>'
                            );
                            ?>
                        </footer><!-- .entry-footer -->
                    </article><!-- #post-## -->
                    <?php $postcount++ ?>
                    
                <?php } elseif( ($postcount % 2) == 1 ) { ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="table">
                    <a class="fontcolor hovercolor relative" href="<?php the_permalink()?>">
                        <div class="tr">
                            <div class="td col half middle">
                                <div class="entry-image">
                                <div class="image">
                                    <?php if (has_post_thumbnail()) { 
                                        the_post_thumbnail('large'); ?>
<!--                                   <img src="http://localhost:8888/disainkleebis/wp-content/uploads/2018/07/aknakleebised-remax-3-1024x644.jpg">-->
                                  <?php  } else {?>
                                    <img src="<?php echo get_template_directory_uri() . '/images/thumbnail-461x288.jpg';?>" alt="placeholder">
                                    <?php   } ?>
                                </div>
                                </div>
                            </div>
                            <div class="td col half middle">        
                                <div class="entry-head leftalign reversepadding">
                                    <?php
                                    if ($first) {
                                        the_title( '<h1 class="specialafter">','</h1>' );
                                    } else {
                                        the_title( '<h2 class="specialafter">','</h2>' );
                                    }           
                                    ?>
                                </div><!-- .entry-header -->
                                <div class="content reversepadding">
                                    <h4 class="leftalign"><?php echo excerpt(20); ?>
                                        <span class="notablet"><span class="button round specialborder maincolor
                          specialcolor">
                        <i class="fa fa-angle-right specialcolor" aria-hidden="true"></i>
                         </span>
                         </span>
                                    </h4>
                        <span class="tablet">
                          <span class="button specialborder fillhover specialcolor
                          specialcolor absolute"> <?php _e('Loe veel', 'mustmuna2017') ?>
                         </span></span>   
                                </div>
                            </div>
                        </div></a>
                    </div>
                    <div class="clear"></div>
                    <footer class="entry-footer">
                        <?php
                        edit_post_link(
                            sprintf(
                                /* translators: %s: Name of current post */
                                __( 'Muuda postitust<span class="screen-reader-text fontcolor hovercolo"> "%s"</span>', 'mustmuna2017' ),
                                get_the_title()
                                ),
                            '<span class="fontcolor hovercolor">',
                            '</span>'
                            );
                            ?>
                        </footer><!-- .entry-footer -->
                    </article><!-- #post-## -->
                <?php $postcount++ ?>
                <?php } ?>
                    <?php
                    $first = false;
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
                <div id="loadmore" class="loadmoreblock">
                    <div id="more_posts" class="loadmore inlineblock specialborder fontcolor hovercolor"><?php _e( 'Lae veel', 'mustmuna2017' )?></div>
                </div>
            </div>
        </div>
    </main>
    <?php get_footer(); ?>
