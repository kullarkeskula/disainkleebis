<?php
/**
 * Template Name: Kontakti leht teistel domeenidle
 */
get_header(); ?>
   <main>
    <section class="map header">
        <div id="map" class="black_map"></div>
        <div class="animate fadeInDown">
            <div class="smallcontact absolute backgroundcolor">
                <?php $stickyphone = str_replace(' ', '', get_field('phone', 'option'));?>
                    <h1 class="contact-smallh1 fontcolor"><?php _e( 'Kontakt', 'mustmuna2017' )?></h1>
                    <div class="get_in_touch_wrapper fontcolor">
                        <div class="mobileinblock">
                            <div class="row"> <span> <i class="fa fa-map-marker fa-stack-1x"></i>
            <?php the_field('address', 'option'); ?> 
          </span> </div>
                            <div class="row">
                                <a class="fontcolor hovercolor" href="tel:<?php echo $stickyphone ?>"> <i class="fa fa-phone fa-stack-1x"></i>
                                    <?php the_field('phone', 'option')?>
                                </a>
                            </div>
                            <div class="row">
                                <a class="fontcolor hovercolor" href="mailto:<?php the_field('email', 'option'); ?>"> <i class="fa fa-envelope fa-stack-1x"></i>
                                    <?php the_field('email', 'option'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>
    <section class="section team darkgray invertmedium fontcolor">
        <div class="lg_container">
            <?php
      if( have_rows('team') ):
    // loop through the rows of data?>
                <div class="half column floatleft">
                    <?php   while ( have_rows('team') ) : the_row();?>
                        <?php if (get_field('about_title')) {?>
                            <h2 class="lessweight"><?php the_field('about_title');?></h2>
                        <?php  } ?>
                            <?php the_field('aboutus') ?>
                        <div class="position">
                            <h4> <?php  the_sub_field('name');?></h4> </div>
                        <div class="phone blackborder">
                            <?php if (get_sub_field('phone')) {?>
                                <?php $phone = str_replace(' ', '', get_sub_field('phone'));
        ?> <span class="tel fontcolor"> <a class="fontcolor hovercolor" href="tel:<?php echo $phone;?>"><i class="fa fa-phone fa-stack-1x"></i><?php the_sub_field('phone') ?></a></span>
                                    <?php } ?>
                        </div>
                        <div class="social">
                            <?php if (get_sub_field('mail')) {?> <span class="fontcolor hovercolor mail icobackground"><a class="hovercolor" href="mailto:<?php  the_sub_field('mail');?>"> <i class="fa fa-envelope fa-stack-1x iconcolor hovercolor"></i><?php  the_sub_field('mail');?></a></span>
                                <?php } ?>
                                    <?php if (get_sub_field('facebook')) {?> <span class="fontcolor hovercolor facebook icobackground"><a class="hovercolor" target="_blank" href="<?php  the_sub_field('facebook');?>"> <i class="fa fa-facebook fa-stack-1x iconcolor hovercolor"></i></a></span>
                                        <?php } ?>
                                            <?php if (get_sub_field('linkedin')) {?> <span class="fontcolor hovercolor linkedin icobackground"><a class="hovercolor" target="_blank" href="<?php  the_sub_field('linkedin');?>"> <i class="fa fa-linkedin fa-stack-1x iconcolor hovercolor"></i></a></span>
                                                <?php  } ?>
                        </div>
                        <?php endwhile;?>
                </div>
                <div class="aboutus half floatleft">
            <div class="call_action call_action-contact">
                <?php if (get_field('contact')) {?>
                    <?php the_field('contact'); ?>
                        <br><div class="contact-form">
                        <?php } ?>
                            <?php the_field('form'); ?></div>
            </div>
                </div>
                <div class="clear"></div>
                <?php endif;?>
        </div>
    </section>
    <script>
        function initMap() {
            // Create a new StyledMapType object, passing it an array of styles,
            // and the name to be displayed on the map type control.
            var styledMapType = new google.maps.StyledMapType(
        	[
                    {
                        "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#f5f5f5"
            }
            ]
          }
                    , {
                        "elementType": "labels.icon"
                        , "stylers": [
                            {
                                "visibility": "off"
            }
            ]
          }
                    , {
                        "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#616161"
            }
            ]
          }
                    , {
                        "elementType": "labels.text.stroke"
                        , "stylers": [
                            {
                                "color": "#f5f5f5"
            }
            ]
          }
                    , {
                        "featureType": "administrative.land_parcel"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#bdbdbd"
            }
            ]
          }
                    , {
                        "featureType": "poi"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#eeeeee"
            }
            ]
          }
                    , {
                        "featureType": "poi"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#757575"
            }
            ]
          }
                    , {
                        "featureType": "poi.business"
                        , "stylers": [
                            {
                                "visibility": "off"
            }
            ]
          }
                    , {
                        "featureType": "poi.park"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#e5e5e5"
            }
            ]
          }
                    , {
                        "featureType": "poi.park"
                        , "elementType": "labels.text"
                        , "stylers": [
                            {
                                "visibility": "off"
            }
            ]
          }
                    , {
                        "featureType": "poi.park"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#9e9e9e"
            }
            ]
          }
                    , {
                        "featureType": "road"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#ffffff"
            }
            ]
          }
                    , {
                        "featureType": "road.arterial"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#757575"
            }
            ]
          }
                    , {
                        "featureType": "road.highway"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#dadada"
            }
            ]
          }
                    , {
                        "featureType": "road.highway"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#616161"
            }
            ]
          }
                    , {
                        "featureType": "road.local"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#9e9e9e"
            }
            ]
          }
                    , {
                        "featureType": "transit.line"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#e5e5e5"
            }
            ]
          }
                    , {
                        "featureType": "transit.station"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#eeeeee"
            }
            ]
          }
                    , {
                        "featureType": "water"
                        , "elementType": "geometry"
                        , "stylers": [
                            {
                                "color": "#c9c9c9"
            }
            ]
          }
                    , {
                        "featureType": "water"
                        , "elementType": "labels.text.fill"
                        , "stylers": [
                            {
                                "color": "#9e9e9e"
            }
            ]
          }
          ], {
                    name: 'Styled Map'
                });
            // Create a map object, and include the MapTypeId to add
            // to the map type control.
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: <?php the_field('lat'); ?>
                    , lng: <?php the_field('long'); ?>
                }
                , zoom: 13
                , scrollwheel: false
                , draggable: true
                , zoomControl: true
                , mapTypeControl: false
                , streetViewControl: false
                , mapTypeControlOptions: {
                    mapTypeIds: ['roadmap'
            , 'styled_map']
                }
            });
            //Associate the styled map with the MapTypeId and set it to display.
            map.mapTypes.set('styled_map', styledMapType);
            map.setMapTypeId('styled_map');
            var image = '<?php echo get_template_directory_uri() ?>/images/reklaamiagentuur_mm_icon.svg';
            var beachMarker = new google.maps.Marker({
                position: {
                    lat: <?php the_field('lat'); ?>
                    , lng: <?php the_field('long'); ?>
                }
                , map: map
                , icon: image
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKmc09m7b3jMXSur_A-Nuv1aW0PVLfbrI&callback=initMap">
    </script>
    </main>
    <?php get_footer(); ?>