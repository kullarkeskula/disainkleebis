<?php
get_header(); ?>
<section class="section fontcolor mediumgray light-inverted">
	<div class="content container defaultpage">
		<?php 		while ( have_posts() ) : the_post();?>
			<?php the_content(); ?>
		<?php endwhile;	?>
	</div>
</section>
<?php get_footer(); ?>