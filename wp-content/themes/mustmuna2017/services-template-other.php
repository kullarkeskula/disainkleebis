<?php
/**
 * Template Name: Võimalused teistel lehtedel
 */
get_header(); ?>
<main>
    <section class="header linebetween specialcolor rightline reverse">
        <div class="bg-holder parallax-windowmin" data-width="1920" data-height="813" data-parallax="3d">
        <img src="<?php echo get_field('hero_image')['url']; ?>" class="parallax" alt="<?php echo get_field('hero_image')['alt']; ?>">
            <div class="absolutehead">
                <div class="table">
                    <div class="tr">
                        <div class="td fontcolor">
                        <div class="minicontainer">
                         <?php if (get_field('header_promo')) {?>
                            <h1 class="centeralign"><?php the_field('header_promo'); ?></h1>
                            <br>
                            <?php } ?>
                            <?php if (get_field('header_extra')) {?>
                            <h4 class="centeralign fontcolor"><?php the_field('header_extra'); ?></h4>
                                <?php } ?>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    if (get_field('gray1') == 'light') {
      $bgcolor = 'light-inverted';
    } elseif (get_field('gray1') == 'dark') {
      $bgcolor = 'dark-inverted';
    } else {
      $bgcolor = 'invertmedium';
    }
    ?>
    <section class="section frontpagepromo promo mediumgray <?php echo $bgcolor; ?> fontcolor">
      <div class="container">
        <?php if (get_field('demo_text')){?>
        <h2 class="centeralign smallerh1"><?php the_field('demo_text'); ?></h2>
        <br>
        <?php } ?>
        <?php if (get_field('promo_1')){?>
        <h4 class="centeralign"><?php the_field('promo_1'); ?></h4>
        <?php } ?>
        <?php if (get_field('promo_cta')){?>
        <h5 class="normal block centeralign"><?php the_field('promo_cta'); ?></h5>
        <?php } ?>
      </div>
    </section>



    <?php if (get_field('half_visibility') == 'extra_visible' ) {?>
    <?php
    if (get_field('gray2') == 'medium') {
      $bgcolor = 'invertmedium';
    } elseif (get_field('gray2') == 'dark') {
      $bgcolor = 'dark-inverted';
    } else {
      $bgcolor = 'light-inverted';
    }
    ?>
    <section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
      <div class="relative">
        <div class="table">
          <div class="tr">
            <?php if ( get_field('halfimg1')){?>   
            <div class="half td middle">
              <div class="minheight">
                <div class="bgimg1 absolute" style="background-image: url(<?php echo get_field('halfimg1')['url'];?>);">
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="half td middle">
              <div class="halfcontent leftpadd leftalign inlineblock">
               <?php if (get_field('half_promo1')){?>
               <h3><?php the_field('half_promo1'); ?></h3>
               <?php } ?>
               <?php if (get_field('half_text1')){?>
               <h4><?php the_field('half_text1'); ?></h4>
               <?php } ?>
               <?php if (get_field('half_cta1')){?>
               <h5 class="normal block"><?php the_field('half_cta1'); ?></h5>
               <?php } ?>
               <?php if (get_field('showbutton1')== 'extra_visible'){?>
               <a href="<?php the_field('halfbutton1') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text') ;?></a>
               <?php } ?>
             </div>
           </div>
         </div>
       </div>

     </div>
    </section>
    <?php } ?>

    <?php if (get_field('half_visibility2') == 'extra_visible' ) {?>
    <?php
    if (get_field('gray3') == 'light') {
      $bgcolor = 'light-inverted';
    } elseif (get_field('gray3') == 'dark') {
      $bgcolor = 'dark-inverted';
    } else {
      $bgcolor = 'invertmedium';
    }
    ?>
    <section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
      <div class="relative">
        <div class="table">
          <div class="tr">

            <div class="half td middle">
              <div class="halfcontent rightpadd leftalign inlineblock">
               <?php if (get_field('half_promo2')){?>
               <h3><?php the_field('half_promo2'); ?></h3>
               <?php } ?>
               <?php if (get_field('half_text2')){?>
               <h4><?php the_field('half_text2'); ?></h4>
               <?php } ?>
               <?php if (get_field('half_cta2')){?>
               <h5 class="normal block"><?php the_field('half_cta2'); ?></h5>
               <?php } ?>
               <?php if (get_field('showbutton2')== 'extra_visible'){?>
               <a href="<?php the_field('halfbutton2') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text2') ;?></a>
               <?php } ?>
             </div>
           </div>
           <?php if ( get_field('halfimg2')){?>   
           <div class="half td middle">
            <div class="minheight">
              <div class="bgimg2 absolute" style="background-image: url(<?php echo get_field('halfimg2')['url'];?>);">

              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>

    </div>
    </section>
    <?php } ?>


    <?php if (get_field('half_visibility3') == 'extra_visible' ) {?>
    <?php
    if (get_field('gray4') == 'medium') {
      $bgcolor = 'invertmedium';
    } elseif (get_field('gray4') == 'dark') {
      $bgcolor = 'dark-inverted';
    } else {
      $bgcolor = 'light-inverted';
    }
    ?>
    <section class="halfpromo darkgray <?php echo $bgcolor; ?> fontcolor">
      <div class="relative">
        <div class="table">
          <div class="tr">
            <?php if ( get_field('halfimg3')){?>   
            <div class="half td middle">
              <div class="minheight">
                <div class="bgimg1 absolute" style="background-image: url(<?php echo get_field('halfimg3')['url'];?>);">
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="half td middle">
              <div class="halfcontent leftpadd leftalign inlineblock">
               <?php if (get_field('half_promo3')){?>
               <h3><?php the_field('half_promo3'); ?></h3>
               <?php } ?>
               <?php if (get_field('half_text3')){?>
               <h4><?php the_field('half_text3'); ?></h4>
               <?php } ?>
               <?php if (get_field('half_cta3')){?>
               <h5 class="normal block"><?php the_field('half_cta3'); ?></h5>
               <?php } ?>
               <?php if (get_field('showbutton3')== 'extra_visible'){?>
               <a href="<?php the_field('halfbutton3') ;?>" class="button smallbutton fillhover specialcolor"><?php the_field('halfbtn_text3') ;?></a>
               <?php } ?>
             </div>
           </div>
         </div>
       </div>

     </div>
    </section>
</main>
<?php } ?>

<?php get_footer(); ?>