<?php
/**
 * Template Name: Võimaluste leht
 */
get_header(); ?>

<section class="header linebetween fontcolor rightline specialcolor reverse">
	<div class="bg-holder parallax-windowmin" data-parallax="3d" data-width="1920" data-height="813" >
		<img src="<?php echo get_field('hero_image')['url']; ?>" class="parallax" alt="<?php echo get_field('hero_image')['alt']; ?>">
		<div class="absolutehead">
			<div class="table">
				<div class="tr">
					<div class="td fontcolor">
					<div class="minicontainer">
					<?php if (get_field('header_promo')) {?>
						<h1 class="centeralign"><?php the_field('header_promo'); ?></h1>
						<br>
							<?php } ?>
						<?php if (get_field('header_extra')) {?>
						<h4 class="centeralign fontcolor"><?php the_field('header_extra'); ?></h4>
							<?php } ?>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
if( have_rows('services') ):?>
<section class="section services mainpage mediumgray invertmedium specialcolor fontcolor">
 <?php	// loop through the rows of data
 while ( have_rows('services') ) : the_row();?>
 <div class="service">
 	<div class="minicontainer">
 		<div class="table">
 			<div class="tr">
 				<div class="td col half middle">
 					<div class="service_text">
 						<h2 class="specialafter leftalign">
 							<?php if (get_sub_field('service_url')) {?>
 							<a class="fontcolor hovercolor" target="_blank" href="<?php the_sub_field('service_url'); ?>"> 
 								<?	} ?>
 								
 								<?php the_sub_field('service_n'); ?>
 								<?php if (get_sub_field('service_url')) {?>	
 							</a>
 							<?	} ?>
 						</h2>
 						<h4 class="leftalign fontcolor"><?php the_sub_field('service_d'); ?></h4>	
 					</div></div>
 					<div class="col half td middle">
 						<?php if (get_sub_field('service_url')) {?>	
 						<a target="_blank" href="<?php the_sub_field('service_url'); ?>" class="button specialborder mobile fontcolor hovercolor">
 							<?php _e('Vaata siit', 'mustmuna2017')?>			
 						</a>
 						<?	} ?>
 						<div class="service_image">
 							<div class="image">
 								<?php if (get_sub_field('service_url')) {?>
 								<a class="fontcolor hovercolor" target="_blank" href="<?php the_sub_field('service_url'); ?>">
 									<?	} ?>
 									<img src="<?php echo get_sub_field('service_image')['url'];?>" alt="<?php echo get_sub_field('service_image')['alt'];?>">
 									<?php if (get_sub_field('service_url')) {?>
 								</a>
 								<?	} ?>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>	
 	</div>
 <?php endwhile;?>
</section>
<?php endif;?>
<?php get_footer(); ?>