<?php
/**
 * Template Name:  Esilehe templiit
 */
get_header(); ?>
<div class="maincontent">
	<div class="header linebetween specialcolor leftline">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="relative demoimg">
				<?php echo get_field('hero_image'); ?>
			</div>
		<?php endwhile; // end of the loop. ?>
	</div>
	<section class="promo section frontpagepromo darkgray invertmedium fontcolor">
		<div class="container">
		  <?php if (get_field('demo_text')){?>
			<h1 class="centeralign"><?php the_field('demo_text'); ?></h1>
			<br>
			      <?php } ?>
      <?php if (get_field('promo_1')){?>
			<h4 class="centeralign"><?php the_field('promo_1'); ?></h4>
			  <?php } ?>
		</div>
	</section>
	<section class="bg-holder frontportfolio linebetween specialcolor rightline sectionpadding" data-parallax="3d">
			<img src="<?php the_field('portfolio_bg'); ?>" class="parallax">
			<div class="container">
			   <?php if (get_field('portfolio_text')) {?>
				<div class="portfoliohead fontcolor">
					<h4 class="centeralign"><?php the_field('portfolio_text'); ?></h4>
				</div>
					  <?php } ?>
				<?php 
				$args = array(
					'post_type' => 'portfolio',
					'post_count' => '12',
					'orderby'   => 'rand',
					'posts_per_page' => 12
					);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) { ?>
				<ul class="portfolio">
					<?php  while ( $the_query->have_posts() ) {
						$the_query->the_post();?>
						<li class="thumbnail">
							<div class="image">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('thumbnail'); ?>
								</a>
							</div>
						</li>
						<?php } ?>
					</ul>
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php } ?>
				</div>
				<div class="clear"></div>
				<div class="centeralign">
					<a target="_blank" href="<?php the_field('portfolio_url'); ?>" class="button smallbutton fillhover specialcolor fontcolor hovercolor">
						<?php _e('Vaata veel', 'mustmuna2017')?>      
					</a>
				</div>
	 

		</section>
		<?php if (get_field('extra_visibility2') == 'extra_visible' ) {?>
  <section class="section frontpagepromo promo darkgray invertmedium fontcolor">
  <div class="container">
  <?php if (get_field('product_text')){?>
    <h1 class="centeralign"><?php the_field('product_text'); ?></h1>
    <br>
      <?php } ?>
      <?php if (get_field('promo_extra')){?>
    <h4 class="centeralign"><?php the_field('promo_extra'); ?></h4>
          <?php } ?>
  </div>
</section>
  <?php } ?>
    <?php if (get_field('extra_visibility3') == 'extra_visible' ) {?>
<section class="section frontpagepromo promo mediumgray dark-inverted fontcolor">
  <div class="container">
  <?php if (get_field('secondproduct_text')){?>
    <h1 class="centeralign"><?php the_field('secondproduct_text'); ?></h1>
    <br>
      <?php } ?>
      <?php if (get_field('secondpromo')){?>
    <h4 class="centeralign"><?php the_field('secondpromo'); ?></h4>
          <?php } ?>
  </div>
</section>
  <?php } ?>
	</div>
	<?php get_footer(); ?>