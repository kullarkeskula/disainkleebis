<?php
function mustmuna2017_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Widget Area', 'mustmuna2017' ),
    'id'            => 'sidebar-1',
    'description'   => __( 'Widgetid, ehk kasutame lisaarendustes', 'mustmuna2017' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'mustmuna2017_widgets_init' );

function register_my_menu()
{
  register_nav_menu('header-menu', 'Header Menu');
}
add_action('init', 'register_my_menu');
add_theme_support('post-thumbnails');

function scripts() {

  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
  wp_enqueue_script( 'jquery' );
  wp_register_script( 'jquery-migrate', includes_url( '/js/jquery/jquery-migrate.js' ), false, NULL, true );
  wp_enqueue_script( 'jquery-migrate' );

  // Load our main stylesheet.
  if (get_theme_mod('sheme')=='light' ) {
    wp_enqueue_style( 'white', get_template_directory_uri() . '/light.css', array(), null );
      
  } 
  else {
    wp_enqueue_style( 'main', get_template_directory_uri() . '/screen.css', array(), null );
  }
    if (is_page_template('landing-template.php'|| is_page_template('landing-packages.php' ))) {
       wp_enqueue_script( 'fancyscript', get_template_directory_uri() . '/js/fancybox/dist/jquery.fancybox.js', array( 'jquery' ) , null , true);
     wp_enqueue_script( 'fancyfire', get_template_directory_uri() . '/js/fancyfire.js', array( 'jquery' ) , null , true);
    wp_enqueue_style( 'fancystile', get_template_directory_uri() . '/js/fancybox/dist/jquery.fancybox.css', array(), null );
  }


  wp_enqueue_script( 'owl', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array( 'jquery' ) , null , true);
     //if is singular portfolio template
  if (is_singular( 'portfolio' ) || is_singular( 'post' )) {
    wp_enqueue_script( 'sticky', get_template_directory_uri() . '/js/sticky.js', array( 'jquery' ), null , true );
    wp_enqueue_script( 'stikyfire', get_template_directory_uri() . '/js/stickyfire.js', array( 'jquery' ), null , true );
  }
  wp_enqueue_script( 'smooth', get_template_directory_uri() . '/js/smoothscroll.js', array( 'jquery' ) , null , true);
  
  wp_enqueue_script( 'parallax', get_template_directory_uri() . '/js/parallax.js', array( 'jquery' ), null , true );

  wp_enqueue_script( 'func', get_template_directory_uri() . '/js/functions-min.js', array( 'jquery' ) , null , true);
  //if is front page remove contact form 7
  if (is_front_page()) {
   wp_deregister_style( 'contact-form-7' );
 }
  //if is posts page use ajax
 if (is_home()) {
  wp_enqueue_script( 'ajax', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ), null , true );
  wp_localize_script( 'ajax', 'ajax_posts', array(
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    'loadmore' => esc_html__('Lae veel', 'mustmuna2017'),
    'noposts' => __('Rohkem postitusi ei leitud', 'mustmuna2017'),
    ));
}
}
add_action( 'wp_enqueue_scripts', 'scripts' );

$args = array(
  // Flex height
  'flex-height' => true,
      // Recommended height
  'height' => 120,
      // Flex width
  'flex-width' => true,
      // Recommended width
  'width' => 120,
      // Template header style callback
  'wp-head-callback' => $wphead_cb,
      // Admin header style callback
  'admin-head-callback' => $adminhead_cb,
      // Admin preview style callback
  'admin-preview-callback' => $adminpreview_cb,
  'default-image' => get_template_directory_uri() . '/images/mm_logo.svg',
  // Template header style callback
  'wp-head-callback' => $wphead_cb,
      // Admin header style callback
  'admin-head-callback' => $adminhead_cb,
      // Admin preview style callback
  'admin-preview-callback' => $adminpreview_cb
  );
add_theme_support( 'custom-header', $args );

function customize_register( $wp_customize ) {

  $wp_customize->add_section(
    'logo_setting_section',
    array(
      'title' => 'Valgete lehtede logo',
      'description' => 'Muuda siit logo',
      'priority' => 55,
      )
    );

    //Add a setting
  $wp_customize->add_setting(
    'logo',
    array(
     'default' => get_template_directory_uri() . '/images/mm_logo.svg',
     'transport'   => 'postMessage'
     )
    );
    //Add control
  $wp_customize->add_control(
   new WP_Customize_Image_Control(
     $wp_customize,
     'logo',
     array(
       'label'      => __( 'Upload a logo', 'theme_name' ),
       'section'    => 'logo_setting_section',
       'settings'   => 'logo'
       )
     )
   );
  $wp_customize->add_section(
    'colorsheme_setting_section',
    array(
      'title' => 'Värviskeem',
      'description' => 'Muuda siit valge/must',
      'priority' => 999,
      )
    );

    //Add a setting
  $wp_customize->add_setting(
    'sheme',
    array(
     'transport'   => 'postMessage',
     'default' => 'dark'
     )
    );
      //Add control
  $wp_customize->add_control(
    'sheme',
    array(
      'label' => 'Vali värviskeem',
      'section' => 'colorsheme_setting_section',
      'settings'       => 'sheme',
      'type'           => 'radio',
      'choices'        => array(
        'dark'   => __( 'Must' ),
        'light'  => __( 'Valge' )
        )

      )
    );
  $wp_customize->add_section(
    'colors_setting_section',
    array(
      'title' => 'Aktsentvärvid',
      'description' => 'Muuda siit valge/must',
      'priority' => 888,
      )
    );

      //Add a setting
  $wp_customize->add_setting(
    'specialcolor',
    array(
     'transport'   => 'postMessage',
     'default' => '#ec008c'
     )
    );
  $wp_customize->add_control( 
    new WP_Customize_Color_Control( 
      $wp_customize, 
      'specialcolor', 
      array(
        'label'      => __( 'Aktsentvärv', 'mustmuna2017' ),
        'section'    => 'colors_setting_section',
        'settings'   => 'specialcolor',
        ) ) 
    );

}
add_action('customize_register', 'customize_register');
function childtheme_customizer_live_preview() {
  wp_enqueue_script(
    'customize_register',     
    get_stylesheet_directory_uri().'/js/theme-customizer.js',
    array( 'jquery','customize-preview' ),  
    '1.0',            
    true            
    );
}
add_action( 'customize_preview_init', 'childtheme_customizer_live_preview' );


// portfoolio
function create_posttype() {
  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __('Portfoolio', 'mustmuna2017'),
        'singular_name' => __('töö', 'mustmuna2017')
        ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => __('portfoolio', 'mustmuna2017')),
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
        )
      )
    );
}

add_action( 'init', 'create_posttype' );
add_action( 'init', 'create_tax' );

function create_tax() {
  register_taxonomy(
    'portfolio_cat',
    'portfolio',
    array(
      'label' => __( 'Portfoolio kategooria', 'mustmuna2017' ),
      'rewrite' => array( 'slug' => __('portfooliokategooria', 'mustmuna2017')),
      'hierarchical' => true,
      )
    );
}

add_action( 'init', 'create_tag_taxonomies', 0 );

//create two taxonomies, genres and tags for the post type "tag"
function create_tag_taxonomies() 
{
  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tags' ),
    'popular_items' => __( 'Popular Tags' ),
    'all_items' => __( 'All Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Tag' ), 
    'update_item' => __( 'Update Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove tags' ),
    'choose_from_most_used' => __( 'Choose from the most used tags' ),
    'menu_name' => __( 'Tags' ),
    ); 

    register_taxonomy('portf_tag','portfolio',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => __('portfolio-silt', 'mustmuna2017') ),
    )
  );
}

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page();
  
}

add_filter( 'wp_check_filetype_and_ext', function($data) {
  if(isset($data['ext'])) {
    if($data['ext'] === 'svg') {
      $data['type'] = 'image/svg+xml';
    }
  }
  return $data;
} );

function more_post_ajax(){

  $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 3;
  $offset = (isset($_POST["offset"])) ? $_POST["offset"] : 3;
  $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

  header("Content-Type: text/html");

  $args = array(
    'suppress_filters' => true,
    'post_type' => 'post',
    'posts_per_page' => $ppp,
    'paged'    => $page,
    'offset'          => $offset
    );

  $loop = new WP_Query($args);

  $out = '';

  if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
  $out .= '<article id="post-'. get_the_ID().'" class="post">
  <div class="table">
    <a class="fontcolor hovercolor" href="'. get_permalink().'">
      <div class="tr">
      <div class="td col half middle">
          <div class="entry-head leftalign"><h2 class="specialafter">
            '.get_the_title().'</h2>
          </div><!-- .entry-header -->
          <div class="content">
            <h4 class="leftalign">'. excerpt( 19 ).'<span class="notablet"><span class="button round specialborder maincolor specialcolor"><i class="fa fa-angle-right specialcolor" aria-hidden="true"></i> </span></span></h4>
            <span class="tablet"><span class="button specialborder fillhover specialcolor specialcolor absolute"> '. __("Loe veel", "mustmuna2017").'</span></span>
          </div>
        </div>
        <div class="td col half middle">
          <div class="entry-image"><div class="image">';
            $thumbnail = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID()): get_template_directory_uri().'/images/thumbnail-461x288.jpg'; 

            if ( has_post_thumbnail() ) {
              $img_id = get_post_thumbnail_id(get_the_ID());
              $alt_text = get_post_meta( $img_id, '_wp_attachment_image_alt', true ) ?  get_post_meta( $img_id, '_wp_attachment_image_alt', true ): 'placeholder';
            }

            $out .= '<img src="'.$thumbnail.'" alt="'.$alt_text.'">

          </div></div>
        </div>
      </div></a>
    </div>
    <div class="clear"></div>
  </article><!-- #post-## -->';
  endwhile;
  endif;
  wp_reset_postdata();
  die($out);
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');
add_action( 'wp_ajax_load-filter', 'more_post_ajax' );

// Remove WP version
function wpbeginner_remove_version() {
  return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

// custom CSS
function mytheme_customize_css()
{
  ?>
  <style type="text/css">
    .specialcolor:before,
    .specialafter:after,
    .specialborder,
     .tabs .tab a.pick_pg.current,
     .packlink.current {
      border-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
    }
    .specialcolor,
     .article .sixty h5 {
      color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
    }

    .specialhover:hover:before {
      background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
    }
    input.specialborder:hover {
      background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
      color: #fff;
    }
    .maincolor {
  background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
      color: #fff;
} 
.maincolor i {
      color: #fff;
} 
    .sort.specialborder:hover,
    .sort.specialborder.currentcat {
     color:  <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
   }
   input.specialborder {
    color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
  }

  .iconcolor:hover {
    color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
  }
  .article a {
 color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
}
a:hover .button.fillhover {
    background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
      color: #fff;
    border-color: #fff;
}
a:hover .button.fillhover i {
      color: #fff;
}
.button.fillhover:hover  {
    background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
      color: #fff;
    border-color: #fff;
}
.button.fillhover:hover i {
      color: #fff;
}

 .round.maincolor {
  background-color: transparent;
      color: #000;
      border-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>
} 
 .round.maincolor i {

      color: #000;

} 
a:hover .round.maincolor {
  background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
} 
a:hover .maincolor i {
      color: #000;
} 
#seogroup-embed #seogroup .seo-scorform .form input[type="submit"] {
  background-color: transparent !important;
      color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>  !important;
    border-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>  !important;
}
#seogroup-embed #seogroup .seo-scorform .form input[type="submit"]:hover,
#seogroup-embed #seogroup .seo-scorform .form input[type="submit"]:active {
    background-color: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>  !important;
      color: #fff  !important;
    border-color: #fff  !important;
}
.colorized svg polygon,
.colorized svg path {
fill: <?php echo get_theme_mod('specialcolor', '#ec008c'); ?>;
}
</style>
<?php
}
add_action( 'wp_head', 'mytheme_customize_css');


function crunchify_social_sharing_buttons() {
  global $post;
  if(is_singular() ){
  
    // Get current page URL 
    $crunchifyURL = urlencode(get_permalink());
 
    // Get current page title
    $crunchifyTitle = str_replace( ' ', '%20', get_the_title());
    
    // Get Post Thumbnail for pinterest
    $crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
 
    // Construct sharing URL without using any script
    $twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL.'&amp;via=Crunchify';
    $facebookURL = 'https://www.facebook.com/sharer/sharer.php?s=100&u='.$crunchifyURL;
    $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$crunchifyURL.'&amp;title='.$crunchifyTitle;
 
    // Based on popular demand added Pinterest too
    $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;
 
    // Add sharing button at the end of page/page content
 
    $content .= '<span class="socialshare">';
    $content .= '<a class="twitter relative" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter fa-stack-1x fontcolor hovercolor"></i></a>';
    $content .= '<a class="facebook relative" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook fa-stack-1x fontcolor hovercolor"></i></a>';
    $content .= '<a class="linkedin relative" href="'.$linkedInURL.'" target="_blank"><i class="fa fa-linkedin fa-stack-1x fontcolor hovercolor"></i></a>';
    $content .= '</span>';
    
    return $content;
  }else{
    // if not a post/page then don't include sharing button
    return $content;
  }
};

function wpdocs_custom_excerpt_length( $length ) {
  if (is_singular( 'post' )) {
    return 100;
  }
  elseif (is_front_page()) {
   return 20;
 }
  else {
     return 37;
  }
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return ' ...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);

    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }

    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content); 
    $content = str_replace(']]>', ']]&gt;', $content);

    return $content;
}

function style_every_second_post ( $classes ) {
global $current_class;
$classes[] = $current_class;
$current_class = ( $current_class == 'odd' ) ? 'even' : 'odd';
return $classes;
}
add_filter ( 'post_class' , 'style_every_second_post' );
global $current_class;
$current_class = 'odd';