<article id="post-<?php the_ID(); ?>" class="darkgray article invertmedium fontcolor" >
    <div class="container">
        <div class="maincontent">
        <div class="table main">
            <div class="tr">    
            <div class="forty td verticaltop">
            <div class="postthumb">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( 'thumbnail' );  
                }
                else {?>
                <img src="<?php echo get_template_directory_uri() . '/images/thumb-330-263.jpg';?>" alt="placeholder">
                <?php }
                ?>
                </div>
                <div class="sticky-element">
                <?php 
        $categories = get_the_category();
        $postid[] = get_the_ID();
        $implodedcat;
        $args = [];
        $args = array(
            'post_type' => 'post',
            'post_count' => '3',
            'orderby'   => 'rand',
            'posts_per_page' => 3,
            'post__not_in' => $postid
            );
        $port_args = array(
            'post_type' => 'portfolio',
            'post_count' => '3',
            'orderby'   => 'rand',
            'posts_per_page' => 3
            );
        if (is_array($categories) && count($categories) >0) {
            shuffle($categories);
            $implodedcat =  $categories[0]->slug;
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    =>  $implodedcat
                    ),
                );
            $port_args['tax_query'] = array(
                array(
                    'taxonomy' => 'portfolio_cat',
                    'field'    => 'slug',
                    'terms'    =>  $implodedcat
                    ),
                );
        }
        $basicargs = array(
            'post_type' => 'post',
            'post_count' => '3',
            'orderby'   => 'rand',
            'posts_per_page' => 3,
            'post__not_in' => $postid
            );
        $port_basicargs = array(
            'post_type' => 'portfolio',
            'post_count' => '3',
            'orderby'   => 'rand',
            'posts_per_page' => 3,
            );
?>
            <?php
                    $the_query = new WP_Query( $args );
                    $port_query = new WP_Query( $port_args );
        if ( !$the_query->have_posts() ) {
            $the_query = new WP_Query( $basicargs );
        }
           if ( !$port_query->have_posts() ) {
            $port_query = new WP_Query( $port_basicargs );
        }

        ?>
        <?php if ( $the_query->have_posts() ) { ?>
        <div class="miniposts lgscreen">
        <h3><?php _e('Samast teemast:', 'mustmuna2017')?></h3>
                    <?php
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();?>
                        <div class="other specialborder">
                            <a class="fontcolor" href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                        </div>
                        
                        <?php }
                        echo '<div class="clear"></div>';?>
                   </div> 
                   <?php   } 
                    wp_reset_postdata();
                    ?>
     <?php if ( $port_query->have_posts() ) { ?>
        <div class="portfolioextra-element lgscreen">
        <div class="smallconteiner">
        <h3><?php _e('Valik tehtud töid:', 'mustmuna2017')?></h3>
                    <?php
                    while ( $port_query->have_posts() ) {
                        $port_query->the_post();?>
                                <div class="smallitem floatleft">
                                            <a class="fontcolor block" href="<?php the_permalink(); ?>">
                                                 <?php the_post_thumbnail('thumbnail'); ?>
                                            </a>
                                        </div>
                        
                        <?php }?>
</div>
                   </div> 
                   <?php   }
                    wp_reset_postdata();
                    ?>

            </div>
            </div>
            <div class="sixty bottom td verticaltop">
                
                                            <?php the_title( '<h1>','</h1>' );?>
                        <div class="texts"><h4><?php echo wp_strip_all_tags( get_the_excerpt(), true )?></h4></div>
                        <?php the_content();?>

            </div>
        </div>
    </div>
    <div class="clear"></div>                   </div>
            <div class="authors specialborder">
            <?php $user_email = get_the_author_meta( 'user_email' ); ?>
            <span class="author"><span class="gray"><?php _e('Postituse autor:', 'mustmuna2017')?></span> <a  class="hovercolor" href="mailto:<?php echo  $user_email ?>"> <?php the_author(); ?></a> 
            <span class="slash desktop"> / </span>
            </span>
            <span class="published">
           <span class="gray"> <?php _e('Avaldatud:', 'mustmuna2017')?></span> <span class="date"><?php the_date(); ?>  </span>
            </span>


            <?php echo crunchify_social_sharing_buttons() ?>
        </div>
     <div class="smallscreen  sticky-element">
 <?php if ( $the_query->have_posts() ) { ?>
          
        <div class="miniposts">
        <h3><?php _e('Samast teemast:', 'mustmuna2017')?></h3>
                    <?php
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();?>
                        <div class="other specialborder">
                            <a class="fontcolor" href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                        </div>
                        
                        <?php }
                        echo '<div class="clear"></div>';?>
                   </div> 
                   <?php   } 
                    wp_reset_postdata();
                    ?>
                          <?php if ( $port_query->have_posts() ) { ?>
        <div class="portfolioextra-element">
        <div class="smallconteiner">
        <h3><?php _e('Valik tehtud töid:', 'mustmuna2017')?></h3>
                    <?php
                    while ( $port_query->have_posts() ) {
                        $port_query->the_post();?>
                                <div class="smallitem floatleft">
                                            <a class="fontcolor block" href="<?php the_permalink(); ?>">
                                                 <?php the_post_thumbnail('thumbnail'); ?>
                                            </a>
                                        </div>
                        
                        <?php }?>
                        </div>
                   </div> 
                   <?php   } 
                    wp_reset_postdata();
                    ?>
            </div>
    <footer class="entry-footer">
        <?php
        edit_post_link(
            sprintf(
                /* translators: %s: Name of current post */
                __( 'Muuda postitust<span class="screen-reader-text"> "%s"</span>', 'mustmuna2017' ),
                get_the_title()
                ),
            '<span class="edit-link">',
            '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
<div class="clear"></div>