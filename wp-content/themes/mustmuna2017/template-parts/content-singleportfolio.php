<article id="post-<?php the_ID(); ?>" class="darkgray article light-inverted fontcolor" >
	<div class="container">
		<div class="tablet">
			<div class="top">
				<div class="content">
					<?php the_title( '<h1>','</h1>' );?>
					<?php           
					$posttags = get_the_terms(get_the_ID(),'portf_tag');
					if ($posttags) {?>  
					<div class="tags marginbottom">
						
						<?php
						foreach($posttags as $tag) {
							$link = get_term_link( $tag->slug, 'portf_tag' );
							echo '<span class="specialcolor"><a href="'.$link.'"> #'.$tag->name . '</a></span>';
						}?>
					</div>
					<?php   }?>
					<div class="texts"><?php the_content();?></div>
				</div>
			</div>
		</div>
		<div class="table main">
			<div class="tr">	
				<div class="sixty td verticaltop">
					<?php
					$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
					$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);	?>
					<?php 
					$images = get_field('gallery');
					if( $images ): ?>
					<?php foreach( $images as $image ): ?>
						<div class="imagethumb">		
							<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if( have_rows('soundcloud_links') ):?>
					<div class="soundcloud_links">
						<?php   while ( have_rows('soundcloud_links') ) : the_row();?>
							<div class="item">
								<?php  the_sub_field('soundcloud');?> 	
							</div>
						<?php endwhile;?>
					</div>
				<?php endif;?>
				<?php if( have_rows('videod') ):?>
					<div class="videos">
						<?php   while ( have_rows('videod') ) : the_row();?>
							<div class="item">
								<?php  the_sub_field('video');?> 	
							</div>
						<?php endwhile;?>
					</div>
				<?php endif;?>
			</div>
			<div class="forty bottom td verticaltop">
				<div class="sticky-element">
					<div class="content">
						<?php the_title( '<h1 class="notablet">','</h1>' );?>
						<div class="texts notablet">
							<?php           
							$posttags = get_the_terms(get_the_ID(),'portf_tag');
							if ($posttags) {?>  
							<div class="tags marginbottom">
								
								<?php
								foreach($posttags as $tag) {
									$link = get_term_link( $tag->slug, 'portf_tag' );
									echo '<span class="specialcolor"><a href="'.$link.'"> #'.$tag->name . '</a></span>';
								}?>
							</div>
							<?php   }?>
							<?php the_content();?></div>
							<?php echo crunchify_social_sharing_buttons() ?>
							<div class="clear"></div>
							<div class="authors specialborder">
								<?php $first=true;?>
								<?php  while ( have_rows('authors') ) : the_row();?>
									<?php
									$position_et = get_sub_field('position_et');
									$maker = get_sub_field('maker');
									?>
									<span class="maker-<?php echo $position_et; ?>">
										<?php if (!$first) {?>
										<span class="slash desktop"><?php echo " / ";?></span>
										<?php } ?> <?php echo $position_et ?>: <a class="hovercolor" href="mailto:<?php echo $maker['value']; ?>"> <?php echo $maker['label']; ?></a>
									</span>
									<?php $first=false;?>
								<?php endwhile;?>
								<?php if (get_field('client')) {?>
								
								<span class="client"><span class="slash desktop"><?php echo " / ";?></span><?php _e('Klient:', 'mustmuna2017')?> <a target="_blank" class="hovercolor" href="<?php the_field('client_url'); ?>"><?php the_field('client'); ?></a></span>
								<?php	} ?>
							</div>
							<div class="portfolioextra-element">
								<div class="smallconteiner">
									<?php 
									$categories = get_the_terms(get_the_ID(), 'portfolio_cat');
									$postid[] = get_the_ID();
									
									$implodedcat;
									$port_args = array(
										'post_type' => 'portfolio',
										'post_count' => '3',
										'orderby'   => 'rand',
										'posts_per_page' => 3,
										'post__not_in' => $postid
										);
									if (is_array($categories) && count($categories) >0) {
										shuffle($categories);
										$implodedcat =  $categories[0]->slug;

										$port_args['tax_query'] = array(
											array(
												'taxonomy' => 'portfolio_cat',
												'field'    => 'slug',
												'terms'    =>  $implodedcat
												),
											);
									}
									$port_basicargs = array(
										'post_type' => 'portfolio',
										'post_count' => '3',
										'orderby'   => 'rand',
										'posts_per_page' => 3,
										'post__not_in' => $postid
										);
										?>
										<?php
										$port_query = new WP_Query( $port_args );
										if ( !$port_query->have_posts() ) {
											$port_query = new WP_Query( $port_basicargs );
										}

										?>

										<?php if ( $port_query->have_posts() ) { ?>
										
										<h3><?php _e('Sama kategooria tööd:', 'mustmuna2017')?></h3>
										<?php
										while ( $port_query->have_posts() ) {
											$port_query->the_post();?>
											
											<div class="smallitem floatleft">
												<a class="fontcolor block" href="<?php the_permalink(); ?>">
													<?php the_post_thumbnail('thumbnail'); ?>
												</a>
											</div>
											
											
											<?php }?>
											
											<?php   } 
											wp_reset_postdata();
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>

				
				
				<footer class="entry-footer">
					<?php
					edit_post_link(
						sprintf(
							/* translators: %s: Name of current post */
							__( 'Muuda postitust<span class="screen-reader-text"> "%s"</span>', 'mustmuna2017' ),
							get_the_title()
							),
						'<span class="edit-link">',
						'</span>'
						);
						?>
					</footer><!-- .entry-footer -->
				</div>
				<?php 
				if (is_array($categories) && count($categories) >0) {
					shuffle($categories);
					$implodedcat =  $categories[0]->term_id;?>

					<?php if(category_description($implodedcat )) {?>
					<section class="section mediumgray invertmedium linebetween specialcolor rightline reverse">
						<div class="container">

							<div class="cat_description fontcolor">
								<?php echo category_description($implodedcat ) ?>
							</div>
						</div>
					</section>
					<?}?>
					

					<?php	}?>
				</article><!-- #post-## -->
				<div class="clear"></div>