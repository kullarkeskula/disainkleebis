<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
  <title><?php wp_title(); ?></title>
  <?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-106052120-4', 'auto');
  ga('send', 'pageview');

</script>

<?php $headcta = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'vorm' ?>

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    ga('send', 'event', '<?php echo $headcta ?>', 'submit', 'kontaktivorm');
console.log('saatsin');
}, false );
</script>
</head>
<body <?php body_class(); ?>>
  <div id="page" class="site">
    <?php if (get_field('is_on', 'option')) {?>
    <div class="contact-us-trigger absolute">
      <div class="contactuse">
        <?php $stickyphone = str_replace(' ', '', get_field('phone', 'option'));?>
        <span class="fa-lg fa-2x">
          <a class="mail" href="mailto:<?php the_field('email', 'option'); ?>"> 
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            width="26px" height="25.999px" viewBox="0 0 26 25.999" enable-background="new 0 0 26 25.999" xml:space="preserve">
            <polygon fill="#000" points="13.051,15.334 0.645,4.578 25.458,4.578 "/>
            <polygon fill="#000" points="9.802,13.502 0.02,21.984 0.02,5.021 "/>
            <polygon fill="#000" points="10.37,13.995 13.051,16.32 15.732,13.996 25.458,22.427 0.645,22.427 "/>
            <polygon fill="#000" points="16.301,13.503 26.083,5.021 26.083,21.984 "/>
          </svg>
        </a></span>
        <span id="nks-tab-2" class="fa-lg fa-2x">
          <a  class="tel" href="tel:<?php echo $stickyphone ?>">
           <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           width="26.22px" height="25.82px" viewBox="0 0 26.22 25.82" enable-background="new 0 0 26.22 25.82" xml:space="preserve">
           <path fill="#000" d="M25.854,20.341l-5.19-5.064c-0.653-0.634-1.771-0.62-2.405,0.029l-0.186,0.188
           c-0.549,0.549-1.213,1.213-1.446,2.346c-2.905-2.096-5.576-4.705-7.952-7.769c1.117-0.261,1.762-0.938,2.294-1.497l0.188-0.195
           c0.654-0.67,0.641-1.749-0.03-2.403l-5.66-5.522C5.159,0.152,4.799,0,4.396,0C2.997,0,1.629,1.809,0.883,3.357
           C0.048,5.091-0.464,7.42,0.601,8.757c5.2,6.526,10.854,12.042,16.806,16.393c0.609,0.444,1.352,0.671,2.208,0.671
           c2.623,0,5.742-2.145,6.456-3.899C26.328,21.29,26.25,20.729,25.854,20.341z"/>
         </svg>
       </a> 
     </span>
     <?php if (get_field('facebook', 'option')) {?>
     <span class="fa-lg fa-2x">
      <a target="_blank" class="fb" href="<?php  echo get_field('facebook', 'option')?> "> 
       <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       width="26px" height="25.999px" viewBox="0 0 26 25.999" enable-background="new 0 0 26 25.999" xml:space="preserve">
       <path fill="#000" d="M20.569,25.999h-6.401l-0.019-11.241h3.362v-2.703h-3.362v-2.01c0-0.908,0.738-1.646,1.646-1.646h1.716
       V5.104h-3.464c-2.111,0-3.828,1.717-3.828,3.828v3.123H8.493v2.703h1.726v11.241H5.436C2.438,25.999,0,23.563,0,20.567V5.432
       C0,2.436,2.438,0,5.436,0h15.133C23.563,0,26,2.436,26,5.432v15.135C26,23.563,23.563,25.999,20.569,25.999z"/>
     </svg>
   </a>
 </span>
 <?php } ?>
 <?php if (get_field('instagram', 'option')) {?>
 <span class="fa-lg fa-2x">
   <a target="_blank" class="instagram" href="<?php  echo get_field('instagram', 'option')?> ">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    width="26px" height="25.999px" viewBox="0 0 26 25.999" enable-background="new 0 0 26 25.999" xml:space="preserve">
    <path fill="#000" d="M12.999,16.653c-2.014,0-3.652-1.639-3.652-3.653s1.638-3.653,3.652-3.653c2.015,0,3.654,1.639,3.654,3.653
    S15.014,16.653,12.999,16.653z"/>
    <path fill="#000" d="M21.002,0H4.999c-2.757,0-5,2.243-5,5.001V21c0,2.757,2.243,5,5,5h16.003c2.757,0,4.999-2.243,4.999-5V5
    C26.001,2.243,23.759,0,21.002,0z M13,19.319c-3.523,0-6.388-2.865-6.388-6.389c0-3.522,2.864-6.388,6.388-6.388
    s6.388,2.865,6.388,6.388C19.388,16.454,16.523,19.319,13,19.319z M22.217,8.929c-0.199,0.197-0.408,0.238-0.548,0.238
    c-0.517,0-0.974-0.523-1.608-1.249c-0.275-0.315-0.584-0.668-0.933-1.018c-0.35-0.349-0.703-0.657-1.017-0.933
    c-0.715-0.625-1.232-1.077-1.248-1.582c-0.005-0.147,0.033-0.365,0.24-0.571c0.322-0.323,0.97-0.517,1.735-0.517
    c1.101,0,2.125,0.394,2.813,1.081C23.009,5.74,22.964,8.178,22.217,8.929z"/>
  </svg>
</a></span>
<?php } ?> 
</div>
</div>
<?php } ?>
<header id="masthead" class="site-header headercolor">
	<div class="lg_container">
		<div class="logo">           
      <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
        <?php if (get_theme_mod( 'logo' )) {?>
        <img src="<?php echo get_theme_mod( 'logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        <?php   } else {?>
        <img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        <?php }?>
      </a>
    </div>
    <button type="button" id="navbar-toggle" class="floatright mobile">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar specialborder"></span>
      <span class="icon-bar specialborder"></span>
      <span class="icon-bar specialborder"></span>
    </button>
    <?php wp_nav_menu(array('theme_location' => 'header-menu', 'container_class' => 'main-menu desktop fontcolor')); ?>
    <div class="clear"></div>
  </div>
</header><!-- .site-header -->


